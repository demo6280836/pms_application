﻿namespace PMS.Contract.Commons.Paging;

public class PagingRequestBase
{
    public string? KeyWords { get; set; }
    public int Page { get; set; } = 1;
    public int ItemsPerPage { get; set; } = 3;
    public int Skip => (Page - 1) * ItemsPerPage;
    public int Take { get => ItemsPerPage; }
    public string? SortField { get; set; }
}