﻿namespace PMS.Contract.Commons.Paging;

public class PagingResponseInfo
{
    public int ItemsPerPage { get; set; }
    public int CurrentPage { get; set; }
    public int ToTalPage
    {
        get
        {
            return (ToTalRecord / ItemsPerPage) + ((ToTalRecord % ItemsPerPage) == 0 ? 0 : 1);
        }
        set { }
    }
    public int ToTalRecord { get; set; }

    public bool HasPreviousPage
    {
        get
        {
            return (CurrentPage > 1);
        }
    }
    public bool HasNextPage
    {
        get
        {
            return (CurrentPage < ToTalPage);
        }
    }
}

public class PagingResponse
{
    public PagingResponseInfo Page { get; set; }
}
