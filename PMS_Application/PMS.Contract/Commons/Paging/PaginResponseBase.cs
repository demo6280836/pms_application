﻿namespace PMS.Contract.Commons.Paging;

public class PaginResponseBase
{
    public PagingResponseInfo Paging { get; set; }
}
