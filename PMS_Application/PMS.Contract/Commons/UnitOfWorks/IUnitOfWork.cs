﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PMS.Contract.Commons.UnitOfWorks
{
	public interface IUnitOfWork : IDisposable
	{
		Task<int> SaveChangesAsync();

		//IProductRepository ProductRepository { get; }
	}
}
