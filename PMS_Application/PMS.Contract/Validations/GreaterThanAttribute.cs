﻿using System.ComponentModel.DataAnnotations;

namespace PMS.Contract.Validations;

public class GreaterThanAttribute : ValidationAttribute
{
    private readonly string _otherProperty;

    public GreaterThanAttribute(string otherProperty)
    {
        _otherProperty = otherProperty;
    }

    protected override ValidationResult IsValid(object value, ValidationContext validationContext)
    {
       var otherValue = validationContext.ObjectType.GetProperty(_otherProperty)
            .GetValue(validationContext.ObjectInstance, null);

        if (value is double currentValue && otherValue is double otherDoubleValue)
        {
            if (currentValue <= otherDoubleValue) 
            {
                return new ValidationResult(ErrorMessage);
            }
        }

        return ValidationResult.Success;
    }
}
