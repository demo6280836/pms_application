﻿using PMS.Contract.Commons.UnitOfWorks;
using PMS.Domain.Contents;

namespace PMS.Contract.Systems.Products;

public interface IProductService : IGenericRepository<Product>
{
    Task<ProductResponse> ProductsAsync(ProductRequest request);
    Task<ProductDto> CreateProductAsync(CreateUpdateProduct request);
    Task<ProductDto> GetProductByIdAsync(int id);
    Task<ProductDto> UpdateProductAsync(int id, CreateUpdateProduct request);
}
