﻿

using PMS.Contract.Contents.Variations;
using PMS.Domain.Contents;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace PMS.Contract.Systems.Products;

public class ProductDto
{
    public int Id { get; set; }
    public string Code { get; set; }
    public string Name { get; set; }
    [Display(Name = "Category")]
    public int? CategoryId { get; set; }
    [Display(Name= "Manufacturer")]
    public int? ManufacturerId { get; set; }
    public int Quantity { get; set; }
    [Display(Name = "Date of Manufacture")]
    public DateTime DateOfManufacture { get; set; }
    [Display(Name = "Date of Expiry")]
    public DateTime? Expiry { get; set; }
    public double Price { get; set; }
    public string? Image { get; set; }
    public double? Discount { get; set; }
    public string? CategoryName { get; set; }
    public string? ManufacturerName { get; set; }
    [Display(Name = "Variations")]
    public List<ProductVariationResponse>? ProductVariations { get; set; }
}
