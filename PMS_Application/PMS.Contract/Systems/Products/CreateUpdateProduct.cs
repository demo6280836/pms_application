﻿using PMS.Domain.Contents;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace PMS.Contract.Systems.Products;

public class CreateUpdateProduct
{
    [Required]
    [MinLength(5)]
    public string Name { get; set; }
    [Display(Name ="Category")]
    public int? CategoryId { get; set; }
    [Display(Name ="Manufacturer")]
    public int? ManufacturerId { get; set; }
    public int Quantity { get; set; }
    [Required]
    [StringLength(10, MinimumLength = 3, ErrorMessage = "{0} độ dài từ {2} đến {1}")]
    public string Code { get; set; }
    [Display(Name ="Production Date")]
    public DateTime? ProductionDate { get; set; }
    [Display(Name = "Expire Date")]
    public DateTime? ExpireDate { get; set; }
    [Required]
    [Range(0, int.MaxValue, ErrorMessage = "Please enter a value bigger than {1}")]
    public double Price { get; set; }
    public string? Image { get; set; }
    public double? Discount { get; set; }
    [Display(Name = "Variations")]
    public string Variations { get; set; }
}
