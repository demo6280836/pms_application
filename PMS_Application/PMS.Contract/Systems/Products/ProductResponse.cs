﻿using PMS.Contract.Commons.Paging;

namespace PMS.Contract.Systems.Products;

public class ProductResponse : PagingResponse
{
    public List<ProductDto> Products { get; set; }
}
