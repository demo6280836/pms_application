﻿using PMS.Contract.Commons.Paging;

namespace PMS.Contract.Systems.Products;

public class ProductRequest : PagingRequestBase
{
    public int? CategoryId { get; set; }
    public int? ManufacturerId { get; set; }
}
