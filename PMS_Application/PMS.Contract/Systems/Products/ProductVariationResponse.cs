﻿namespace PMS.Contract.Systems.Products;

public class ProductVariationResponse
{
    public int VariationId { get; set; }
    public string Name { get; set; }
    public string Value { get; set; } 
}
