﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PMS.Contract.Systems.Roles
{
    public interface IRoleService
    {
		Task<RoleResponse> GetListAllPaging(RoleRequest request);

        Task<List<RoleDto>> GetListAllAsync();
        Task AddRole(CreateUpdateRoleDto input);

		Task UpdateRole(string id, CreateUpdateRoleDto input);

		Task DeleteManyRole(string[] ids);
		Task<RoleDto> GetRoleById(string roleId);

		Task<PermissionDto> GetAllRolePermission(string roleId);

		Task SavePermission(PermissionDto input);
	}
}
