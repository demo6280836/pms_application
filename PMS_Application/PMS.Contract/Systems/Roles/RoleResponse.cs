﻿using PMS.Contract.Commons.Paging;

namespace PMS.Contract.Systems.Roles;

public class RoleResponse : PagingResponse
{
    public List<RoleDto> Roles { get; set; }
}
