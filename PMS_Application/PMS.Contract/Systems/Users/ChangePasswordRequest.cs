﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PMS.Contract.Systems.Users
{
	public class ChangePasswordRequest
	{
		public string OldPassword { get; set; }

		public string NewPassword { get; set; }
	}
}
