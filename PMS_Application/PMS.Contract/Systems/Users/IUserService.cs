﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PMS.Contract.Systems.Authentications;

namespace PMS.Contract.Systems.Users
{
    public interface IUserService
    {
        Task DeleteAsync(string id);

        Task<UserResponse> GetListAllAsync(UserRequest request);
        Task AssignRolesAsync(string userId, string[] roleNames);

        Task CreateUser(CreateUserDto userDto);

        Task UpdateUser(string id, UpdateUserDto userDto);

        Task<UserDto> GetUserByIdAsync(string id);


    }
}
