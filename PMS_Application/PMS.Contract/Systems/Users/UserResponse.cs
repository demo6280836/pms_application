﻿using PMS.Contract.Commons.Paging;

namespace PMS.Contract.Systems.Users;

public class UserResponse : PagingResponse
{
    public List<UserDto> Users { get; set; }
}
