﻿using PMS.Contract.Systems.Products;

namespace PMS.Contract.Contents.Order;

public interface IOrderService
{
    Task<OrderDto> OrderProductAsync(IList<ProductOrderRequest> productOrders, OrderRequest orderDto);
    List<ProductDto> GetProductsByCodeAndCheckQuantity(string code, int quantity);

}
