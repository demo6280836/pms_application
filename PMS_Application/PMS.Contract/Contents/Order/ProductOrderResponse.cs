﻿namespace PMS.Contract.Contents.Order;
public class ProductOrderResponse 
{
    public int ProductId { get; set; }
    public string ProductCode { get; set; }
    public double Price { get; set; } 
    public int Quantity { get; set; } // số lượng sp đã mua
}
