﻿namespace PMS.Contract.Contents.Order;

public class ProductOrderRequest
{
    public string Code { get; set; }
    public string? Name { get; set; }
    public int Quantity { get; set; }
    public double Price { get; set; }
}
