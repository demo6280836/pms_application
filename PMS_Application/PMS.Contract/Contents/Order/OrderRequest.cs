﻿using PMS.Contract.Validations;

namespace PMS.Contract.Contents.Order;

public class OrderRequest
{
    public double TotalPrice { get; set; }
    [GreaterThan("TotalPrice", ErrorMessage = "Thiếu tiền bạn ơi")]
    public double? PaymentCustomer { get; set; }
    public double Discount { get; set; }
    public string? CustomerName { get; set; }
    public string? CustomerPhoneNumber { get; set; }
    public string? CustomerAddress { get; set; }
}
