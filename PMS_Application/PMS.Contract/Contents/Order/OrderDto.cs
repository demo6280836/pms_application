﻿using PMS.Domain.Abstracts;
using PMS.Domain.Contents;

namespace PMS.Contract.Contents.Order;

public class OrderDto
{
    public double Total { get; set; }
    public double PaymentCustomer { get; set; }
    public double Discount { get; set; }
    public string? CustomerName { get; set; }
    public string? CustomerPhoneNumber { get; set; }
    public string? CustomerAddress { get; set; }
    public ICollection<OrderItemDto>? OrderItems { get; set; }
}
