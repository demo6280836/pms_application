﻿using PMS.Contract.Commons.UnitOfWorks;
using PMS.Domain.Contents;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PMS.Contract.Contents.Variations;

public interface IVariationService : IGenericRepository<Variation>
{
    
}
