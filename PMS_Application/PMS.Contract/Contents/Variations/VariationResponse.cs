﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PMS.Contract.Contents.Variations;

public class VariationResponse
{
    public string id { get; set; }
    public string label { get; set; }
    public string value { get; set; }
}
