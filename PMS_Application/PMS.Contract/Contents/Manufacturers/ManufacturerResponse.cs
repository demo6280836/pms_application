﻿using PMS.Contract.Commons.Paging;

namespace PMS.Contract.Contents.Manufacturers;

public class ManufacturerResponse : PagingResponse
{
    public List<ManufacturerDto> Manufacturers { get; set; }
}
