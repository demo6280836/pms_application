﻿using PMS.Domain.Contents;

namespace PMS.Contract.Contents.Manufacturers;

public class ManufacturerDto
{
    public int Id { get; set; }
    public string Name { get; set; }
    public string Code { get; set; }
    public string Address { get; set; }
    public string Phone { get; set; }
    public string Email { get; set; }
}
