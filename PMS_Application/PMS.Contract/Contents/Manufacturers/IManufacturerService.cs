﻿using PMS.Contract.Commons.UnitOfWorks;
using PMS.Domain.Contents;

namespace PMS.Contract.Contents.Manufacturers;

public interface IManufacturerService : IGenericRepository<Manufacturer>
{
    public ManufacturerResponse GetManufacturers(ManufacturerRequest request);
}
