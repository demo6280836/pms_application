﻿
using System.ComponentModel.DataAnnotations;

namespace PMS.Contract.Contents.Manufacturers;

public class CreateUpdateManufacturer
{
    [Required]
    [StringLength(100, MinimumLength = 3, ErrorMessage = "{0} phải dài {2} đến {1} ký tự")]
    public string Name { get; set; }
    [Required]
    [StringLength(6, MinimumLength = 3, ErrorMessage = "{0} phải dài {2} đến {1} ký tự")]
    public string Code { get; set; }
    [Required]
    public string Address { get; set; }
    [Required]
    public string Phone { get; set; }
    [Required]
    public string Email { get; set; }
}
