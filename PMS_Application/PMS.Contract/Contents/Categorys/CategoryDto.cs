﻿using PMS.Domain.Contents;

namespace PMS.Contract.Contents.Categorys;

public class CategoryDto
{
    public int Id { get; set; }
    public string Name { get; set; }
    public int? ParentId { get; set; }
    public Category? CategoryParent { get; set; }
}
