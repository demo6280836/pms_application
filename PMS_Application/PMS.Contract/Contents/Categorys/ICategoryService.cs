﻿using PMS.Contract.Commons.UnitOfWorks;
using PMS.Domain.Contents;

namespace PMS.Contract.Contents.Categorys;

public interface ICategoryService : IGenericRepository<Category>
{
    public CategoryResponse GetCategories(CategoryRequest request);
}
