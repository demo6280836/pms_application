﻿
using System.ComponentModel.DataAnnotations;

namespace PMS.Contract.Contents.Categorys;

public class CreateUpdateCategory
{
    [Required]
    [Display(Name = "name of category")]
    [StringLength(50, MinimumLength = 3, ErrorMessage = "{0} phải dài {2} đến {1} ký tự")]
    public string Name { get; set; }
    public int? ParentId { get; set; }
}
