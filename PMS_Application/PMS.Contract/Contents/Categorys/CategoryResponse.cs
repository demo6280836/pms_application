﻿using PMS.Contract.Commons.Paging;

namespace PMS.Contract.Contents.Categorys;

public class CategoryResponse : PagingResponse
{
    public List<CategoryDto> Categories { get; set; }
}
