﻿using PMS.Contract.Signalr.Messages;
using PMS.Domain.Signalr;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PMS.Contract.Signalr.Groups
{
	public class GroupDto
	{
        public int Id { get; set; }
        public string Name { get; set; }
        public string? LastMassage { get; set; }
        public List<MessageDto>? Messages { get; set; }
        public List<GroupUser>? GroupUsers { get; set; }


    }
}
