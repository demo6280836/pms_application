﻿using PMS.Contract.Commons.UnitOfWorks;
using PMS.Domain.Signalr;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PMS.Contract.Signalr.Messages
{
	public interface IMessageService : IGenericRepository<Message>
	{
	}
}
