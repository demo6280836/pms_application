﻿using PMS.Domain.Signalr;
using PMS.Domain.Systems;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PMS.Contract.Signalr.Messages
{
	public class MessageDto
	{
        public int Id { get; set; }
        public string Content { get; set; }
		public string SenderId { get; set; }
		public int GroupId { get; set; }
        public string Username { get; set; }
        public DateTime CreationTime { get; set; }
        public Group Group { get; set; }
		public  AppUser Sender { get; set; }
	}
}
