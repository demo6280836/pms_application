﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PMS.Contract.Signalr.Messages
{
	public class CreateMessageDto
	{
		public string Content { get; set; }
		public string SenderId { get; set; }
		public int GroupId { get; set; }
	}
}
