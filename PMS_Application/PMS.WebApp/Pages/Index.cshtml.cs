﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace PMS.WebApp.Pages
{
	[Authorize]
	public class IndexModel : PageModel
	{
		private readonly ILogger<IndexModel> _logger;

		public IndexModel(ILogger<IndexModel> logger)
		{
			_logger = logger;
		}

        public void OnGet()
		{

		}

		public void OnPost() {

            try
            {
                throw new UnauthorizedAccessException();
            }
            catch (Exception ex)
            {
                ViewData["ErrorMessage"] = ex.Message;

            }

        }
    }
}