﻿using Microsoft.AspNetCore.Authorization;

namespace PMS.WebApp.Authorizations
{
    public class PermissionRequirement : IAuthorizationRequirement
    {
        public string Permission { get; private set; }
        public PermissionRequirement(string permission)
        {
            Permission = permission;
        }
    }
}
