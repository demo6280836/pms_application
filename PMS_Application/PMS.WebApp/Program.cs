using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.UI.Services;
using Microsoft.EntityFrameworkCore;
using PMS.Application;
using PMS.Application.Commons.Emails;
using PMS.Domain.Systems;
using PMS.Infrastructure.EnityFrameworkCore;
using PMS.WebApp.Authorizations;
using PMS.WebApp.Hubs;

namespace PMS.WebApp
{
	public class Program
	{
		public static void Main(string[] args)
		{
			var builder = WebApplication.CreateBuilder(args);

			
			var configuration = builder.Configuration;
			// Add services to the container.
			builder.Services.AddRazorPages();
            var PMSCorsPolicy = "PMSCorsPolicy";
            builder.Services.AddSingleton<IAuthorizationPolicyProvider, PermissionPolicyProvider>();
            builder.Services.AddScoped<IAuthorizationHandler, PermissionAuthorizationHandler>();

            builder.Services.AddCors(o => o.AddPolicy(PMSCorsPolicy, builder =>
            {
                builder.AllowAnyMethod()
                    .AllowAnyHeader()
                    .WithOrigins(configuration["AllowedOrigins"])
                    .AllowCredentials();
            }));
            builder.Services.AddHttpContextAccessor();

			builder.Services.AddDbContext<PMSDbContext>(options =>
			options.UseSqlServer(configuration.GetConnectionString("Default")));

			builder.Services.AddIdentity<AppUser, IdentityRole>()
				 .AddEntityFrameworkStores<PMSDbContext>()
				 .AddDefaultUI()
				 .AddDefaultTokenProviders();


			builder.Services.Configure<IdentityOptions>(options => {
				// Thiết lập về Password
				options.Password.RequireDigit = false; // Không bắt phải có số
				options.Password.RequireLowercase = false; // Không bắt phải có chữ thường
				options.Password.RequireNonAlphanumeric = false; // Không bắt ký tự đặc biệt
				options.Password.RequireUppercase = false; // Không bắt buộc chữ in
				options.Password.RequiredLength = 3; // Số ký tự tối thiểu của password
				options.Password.RequiredUniqueChars = 1; // Số ký tự riêng biệt

				// Cấu hình Lockout - khóa user
				options.Lockout.DefaultLockoutTimeSpan = TimeSpan.FromMinutes(5); // Khóa 5 phút
				options.Lockout.MaxFailedAccessAttempts = 5; // Thất bại 5 lầ thì khóa
				options.Lockout.AllowedForNewUsers = true;

				// Cấu hình về User.
				options.User.AllowedUserNameCharacters = // các ký tự đặt tên user
					"abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789-._@+";
				options.User.RequireUniqueEmail = true;  // Email là duy nhất

				// Cấu hình đăng nhập.
				options.SignIn.RequireConfirmedEmail = true;            // Cấu hình xác thực địa chỉ email (email phải tồn tại)
				options.SignIn.RequireConfirmedPhoneNumber = false; // Xác thực số điện thoại
				options.SignIn.RequireConfirmedAccount = true;

			});

           

            builder.Services.AddAuthentication().AddGoogle(options =>
			{
                IConfigurationSection authenticationSection = configuration.GetSection("Authentication");
                string googleClientId = authenticationSection["Google:ClientId"];
                string googleClientSecret = authenticationSection["Google:ClientSecret"];
                options.ClientId = googleClientId;
				options.ClientSecret = googleClientSecret;
				//options.CallbackPath = "/Auth/Signin-google";
			});
            builder.Services.Configure<EmailConfiguration>(configuration.GetSection("EmailConfiguration"));
			builder.Services.AddSingleton<IEmailSender, SendMailService>();

            builder.Services.ConfigureApplicationServices();

			//builder.Services.Configure<JwtSetting>(configuration.GetSection("JwtSettings"));

			builder.Services.ConfigureApplicationCookie(options =>
			{
				options.LoginPath = "/Auth/Login";
				options.LogoutPath = "/Auth/Logout/";
				options.AccessDeniedPath = "/Auth/AccessDenied";
			});


			builder.Services.AddSignalR();
			builder.Services.AddHttpClient();
			builder.Services.AddControllers();
			// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
			builder.Services.AddEndpointsApiExplorer();
			builder.Services.AddSwaggerGen();

			var app = builder.Build();

			// Configure the HTTP request pipeline.
			if (!app.Environment.IsDevelopment())
			{
				app.UseExceptionHandler("/Error");
				// The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
				app.UseHsts();
			}

			if (app.Environment.IsDevelopment())
			{
				app.UseSwagger();
				app.UseSwaggerUI();

			}

			app.UseHttpsRedirection();
			app.UseStaticFiles();
            app.UseCors(PMSCorsPolicy);
            app.UseRouting();
			app.UseAuthentication();
			app.UseAuthorization();

			app.MapRazorPages();
			app.MapControllers();

			app.MapHub<ChatHub>("/chatHub");

			app.Run();
		}
	}
}