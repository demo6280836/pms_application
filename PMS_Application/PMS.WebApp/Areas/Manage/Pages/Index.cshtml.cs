﻿using DocumentFormat.OpenXml.Bibliography;
using DocumentFormat.OpenXml.Spreadsheet;
using Irony;
using Irony.Parsing;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using PMS.Contract.Contents.Categorys;
using PMS.Contract.Systems.Products;
using PMS.Infrastructure.EnityFrameworkCore;
using System.Globalization;
using System.Text.Json;

namespace PMS.WebApp.Areas.Manage.Pages;

public class IndexModel : PageModel
{
    private readonly PMSDbContext context;
    private readonly ICategoryService categoryService;

    public IndexModel(PMSDbContext context, ICategoryService categoryService)
    {
        this.context = context;
        this.categoryService = categoryService;
    }

    [BindProperty]
    public RevenueResponse RevenueToDay { get; set; }
    [BindProperty]
    public double RateYesterday { get; set; }
    [BindProperty]
    public RevenueResponse RevenueWeek { get; set; }
    [BindProperty]
    public double RateLastWeek { get; set; }
    [BindProperty]
    public RevenueResponse RevenueQuarter { get; set; }
    [BindProperty]
    public double RateLastQuarter { get; set; }
    [BindProperty]
    public RevenueResponse RevenueYear { get; set; }
    [BindProperty]
    public double RateLastYear { get; set; }

    public List<RevenueResponse> RevenueByMonthList { get; set; }

    public int YearRequest { get; set; } = DateTime.Now.Year;
    public int MonthRequest { get; set; } = DateTime.Now.Month;

    public List<int> YearList { get; set; }

    public List<ProductRevenueTop> ProductRevenueTops { get; set; }
    public List<ProductRevenueTop> ProductQuantityTops { get; set; }

    public List<CategoryRevenue> ListCategory { get; set; }


    public void OnGet()
    {
        var now = DateTime.Now;
        YearList = GetYears();
        CalculateRevenue(now);
        RevenueByMonthList = RevenueByMonth(YearRequest);
        ProductRevenueTops = GetTopSellingProduct(MonthRequest, YearRequest, "", false);
        ProductQuantityTops = GetTopSellingProduct(MonthRequest, YearRequest, "", true);
        ListCategory = GetCategoryStatistic(MonthRequest, YearRequest);
    }

    public List<CategoryRevenue> GetCategoryStatistic(int month, int year)
    {
        var result = context.Categories
            .Include(c => c.Products)
            .ThenInclude(p => p.OrderItems)
            .Select(category => new CategoryRevenue
            {
                CategoryId = category.Id,
                CategoryName = category.Name,
                SoldProduct = category.Products
                    .SelectMany(p => p.OrderItems)
                    .Sum(orderItem => orderItem.Quantity),
                StockProduct = category.Products.Sum(p => p.Quantity)
            })
            .ToList();

        return result;
    }


    [HttpGet]
    public IActionResult OnGetChartData(int month, int year)
    {
        ProductRevenueTops = GetTopSellingProduct(month, year, "", false);
        ProductQuantityTops = GetTopSellingProduct(month, year, "", true);
        YearRequest = year;
        MonthRequest = month;

        // Trả về kết quả (có thể là JSON, PartialView, hoặc ActionResult khác)
        return new JsonResult(new { Quantity = ProductQuantityTops, Money = ProductRevenueTops });
    }


    // isQuantity check get top quantity or money
    private List<ProductRevenueTop> GetTopSellingProduct(int month, int year, string sort, bool isQuantity)
    {
        var product = context.OrderItems
            .Include(x => x.Product)
            .Where(x => x.CreationTime.Value.Month == month
                && x.CreationTime.Value.Year == year)
            .GroupBy(od => od.Product.Code)
            .Select(g => new ProductRevenueTop
            {
                ProductCode = g.Key,
                ProductName = context.Products.FirstOrDefault(p => p.Code == g.Key).Name,
                TotalQuantity = g.Sum(od => od.Quantity),
                TotalMoney = g.Sum(od => od.Quantity * od.Price)
            });
        if (isQuantity)
        {
            product = product.OrderByDescending(x => x.TotalQuantity);
        }
        else
        {
            product = product.OrderByDescending(x => x.TotalMoney);
        }

        return product.Take(5).ToList();
    }


    private double RateDiff(double revenuePresent, double revenuePast)
     => revenuePast != 0 ? Math.Round(((revenuePresent - revenuePast) / revenuePast * 100), 2) : 100.0;

    private DateTime FindDayOfWeek(DateTime dateTime, DayOfWeek dayOfWeek)
    {
        while (dateTime.DayOfWeek != dayOfWeek)
        {
            dateTime = dateTime.AddDays(-1);
        }

        return dateTime;
    }


    private RevenueResponse Revenue(DateTime fromDate, DateTime toDate)
    {
        var totalMoney = context.Orders
            .Where(x => x.CreationTime.Value.Date >= fromDate.Date
                && x.CreationTime.Value.Date <= toDate.Date)
            .Sum(x => x.Total);

        var quantitySale = context.OrderItems
            .Where(x => x.CreationTime.Value.Date >= fromDate.Date && x.CreationTime.Value.Date <= toDate.Date)
            .Sum(x => x.Quantity);


        return new RevenueResponse
        {
            Quantity = quantitySale,
            Money = totalMoney,
        };
    }


    private List<RevenueResponse> RevenueByMonth(int year)
    {
        var months = new List<string>
        { "Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec" };
        var result = months.Select(month => new RevenueResponse
        {
            Label = month,
            Money = context.Orders
                .Where(x => x.CreationTime.HasValue
                && x.CreationTime.Value.Year == year
                && x.CreationTime.Value.Month == DateTime.ParseExact(month, "MMM", CultureInfo.InvariantCulture).Month)
                .Sum(x => x.Total),
            Quantity = context.OrderItems
                .Where(x => x.CreationTime.HasValue
                && x.CreationTime.Value.Year == year
                && x.CreationTime.Value.Month == DateTime.ParseExact(month, "MMM", CultureInfo.InvariantCulture).Month)
                .Sum(x => x.Quantity)
        }).ToList();

        return result;
    }

    // get all year in database
    private List<int> GetYears()
    {
        var yearsOfOrder = context.Orders
                .Select(x => x.CreationTime.Value.Year)
                .Distinct()
                .ToList();
        var yearsOfProduct = context.Products
            .Select(x => x.CreationTime.Value.Year)
                .Distinct()
                .ToList();

        return yearsOfProduct.Union(yearsOfOrder).ToList();
    }

    private void CalculateRevenue(DateTime now)
    {
        // revenue day
        RevenueToDay = Revenue(now, now);
        // calculate revenue of yesterday
        var revenueYesterday = Revenue(now.AddDays(-1), now.AddDays(-1)).Money;

        RateYesterday = RateDiff(RevenueToDay.Money, revenueYesterday);
        // end revenue day

        // revenue week
        var mondayOfWeek = FindDayOfWeek(now, DayOfWeek.Monday);
        RevenueWeek = Revenue(mondayOfWeek, now);
        // calculate revenue of last week
        var revenuePreviousWeek = Revenue(mondayOfWeek.AddDays(-8), mondayOfWeek.AddDays(-1)).Money;
        RateLastWeek = RateDiff(RevenueWeek.Money, revenuePreviousWeek);
        // end revenue week

        // revenue quarter
        var quarterNow = (now.Month - 1) / 3 + 1;
        var firstMonthOfQuarterNow = (quarterNow - 1) * 3 + 1;
        var lastDayOfQuarterNow = DateTime.DaysInMonth(now.Year, quarterNow * 3);

        // calculate first date and last date of this now quarter
        var firstDateOfQuarterNow = new DateTime(now.Year, firstMonthOfQuarterNow, 1);
        var lastDateOfQuarterNow = new DateTime(now.Year, quarterNow * 3, lastDayOfQuarterNow);

        RevenueQuarter = Revenue(firstDateOfQuarterNow, now);

        // calculate revenue of last quarter
        var firstMonthOfPreviousQuarter = (quarterNow - 2) * 3 + 1;
        var lastDayOfPreviousQuarter = DateTime.DaysInMonth(now.Year, (quarterNow - 1) * 3);

        // calculate first date and last date of this last quarter
        var firstDateOfPreviousQuarter = new DateTime(now.Year, firstMonthOfPreviousQuarter, 1);
        var lastDateOfPreviousQuarter = new DateTime(now.Year, (quarterNow - 1) * 3, lastDayOfPreviousQuarter);

        var revenuePreviousQuarter = Revenue(firstDateOfPreviousQuarter, lastDateOfPreviousQuarter);

        RateLastQuarter = RateDiff(RevenueQuarter.Money, revenuePreviousQuarter.Money);
        // end revenue quarter


        // revenue year
        RevenueYear = Revenue(new DateTime(now.Year, 1, 1), now);
        var renenuwYear = Revenue(new DateTime(now.Year - 1, 1, 1),
            new DateTime(now.Year - 1, 12, 31)).Money;
        RateLastYear = RateDiff(RevenueYear.Money, renenuwYear);
        // end revenue year
    }
}

public class RevenueResponse
{
    public string? Label { get; set; }
    public int Quantity { get; set; }
    public double Money { get; set; }
}

public class ProductRevenueTop
{
    public string ProductCode { get; set; }
    public string? ProductName { get; set; }
    public int TotalQuantity { get; set; }
    public double TotalMoney { get; set; }
}


public class CategoryRevenue
{
    public int CategoryId { get; set; }
    public string CategoryName { get; set; }
    public int StockProduct { get; set; }
    public int SoldProduct { get; set; }
}