﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using PMS.Contract.Commons.UnitOfWorks;
using PMS.Contract.Contents.Categorys;
using PMS.Contract.Contents.Manufacturers;
using PMS.Contract.Contents.Variations;
using PMS.Contract.Systems.Products;
using PMS.Domain.Contents;

namespace PMS.WebApp.Areas.Manage.Pages.Product
{
    public class CreateModel : ProductPageModel
    {
        
        public CreateModel(
            IProductService productService,
            ICategoryService categoryService, 
            IManufacturerService manufacturerService, 
            IVariationService variationService,
            IUnitOfWork unitOfWork) 
            : base(productService, categoryService, manufacturerService, unitOfWork, variationService)
        {
        }

        [BindProperty(SupportsGet = true)]
        public CreateUpdateProduct ProductRequest { get; set; }

        [BindProperty(SupportsGet = true)]
        public List<PMS.Domain.Contents.Category> Category { get; set; }
        [BindProperty(SupportsGet = true)]
        public List<PMS.Domain.Contents.Manufacturer> Manufacturer { get; set; }
        [BindProperty]
        public List<PMS.Domain.Contents.Variation> Variations { get; set; }

        public async Task OnGetAsync()
        {
            Category = await categoryService.GetListAsync().ToListAsync();
            Manufacturer = await manufacturerService.GetListAsync().ToListAsync();
            Variations = await variationService.GetListAsync().ToListAsync();

        }

        public async Task<IActionResult> OnPostAsync()
        {
            try
            {
                var product = await productService.CreateProductAsync(ProductRequest);
                if (product != null)
                {
                    StatusMessage= $"Bạn vừa tạo product mới: {ProductRequest.Name}";
                    return RedirectToPage("./Index");
                }
            }
            catch (Exception ex)
            {
                ViewData["ErrorMessage"] = ex.Message;
            }
            return Page();
        }
    }
}
