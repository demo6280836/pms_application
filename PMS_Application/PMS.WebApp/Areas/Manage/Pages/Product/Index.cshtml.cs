using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using PMS.Contract.Commons.UnitOfWorks;
using PMS.Contract.Contents.Categorys;
using PMS.Contract.Contents.Manufacturers;
using PMS.Contract.Systems.Products;
using PMS.Domain.Contents;
using PMS.WebApp.Areas.Manage.Pages.Product;

namespace PMS.WebApp.Areas.Manage.Pages.Inventory.Product
{
    public class IndexModel : ProductPageModel
    {
        public IndexModel(IProductService productService,
            ICategoryService categoryService,
            IManufacturerService manufacturerService,
            IUnitOfWork unitOfWork) 
            : base(productService, categoryService, manufacturerService, unitOfWork)
        {
        }

        [BindProperty(SupportsGet = true)]
        public ProductResponse ProductReponse { get; set; }
        [BindProperty(SupportsGet = true)]
        public ProductRequest ProductRequest { get; set; }
        // dropdown
        [BindProperty(SupportsGet = true)]
        public List<PMS.Domain.Contents.Category> Category { get; set; }
        [BindProperty(SupportsGet = true)]
        public List<PMS.Domain.Contents.Manufacturer> Manufacturer { get; set; }


        public async Task<IActionResult> OnGetAsync()
        {
            ProductReponse = await productService.ProductsAsync(ProductRequest);
            Category = await categoryService.GetListAsync().ToListAsync();
            Manufacturer = await manufacturerService.GetListAsync().ToListAsync();  
            return Page();
        }

        public async Task<IActionResult> OnPostAsync()
        {
            Category = await categoryService.GetListAsync().ToListAsync();
            Manufacturer = await manufacturerService.GetListAsync().ToListAsync();
            ProductReponse = await productService.ProductsAsync(ProductRequest);
            return Page();
        }
    }
}
