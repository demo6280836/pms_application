﻿using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using PMS.Application.Contents;
using PMS.Contract.Commons.UnitOfWorks;
using PMS.Contract.Contents.Categorys;
using PMS.Contract.Contents.Manufacturers;
using PMS.Contract.Contents.Variations;
using PMS.Contract.Systems.Products;
using PMS.Domain.Contents;

namespace PMS.WebApp.Areas.Manage.Pages.Product
{
    public class DetailModel : ProductPageModel
    {
        private readonly IMapper mapper;
        public DetailModel(
            IMapper mapper,
            IProductService productService, 
            ICategoryService categoryService, 
            IManufacturerService manufacturerService, 
            IVariationService variationService,
            IUnitOfWork unitOfWork) 
            : base(productService, categoryService, manufacturerService, unitOfWork, variationService)
        {
            this.mapper = mapper;
        }

        [BindProperty]
        public string VariationJson { get; set; }

        [BindProperty]
        public ProductDto ProductDto { get; set; }
        [BindProperty]
        public CreateUpdateProduct CreateUpdateProduct { get; set; }
        [BindProperty(SupportsGet = true)]
        public List<PMS.Domain.Contents.Category> Category { get; set; }
        [BindProperty(SupportsGet = true)]
        public List<PMS.Domain.Contents.Manufacturer> Manufacturer { get; set; }
        [BindProperty]
        public List<PMS.Domain.Contents.Variation> Variations { get; set; }

        public async Task<IActionResult> OnGetAsync(int id)
        {

            Category = await categoryService.GetListAsync().ToListAsync();
            Manufacturer = await manufacturerService.GetListAsync().ToListAsync();
            Variations = await variationService.GetListAsync().ToListAsync();
            ProductDto = await productService.GetProductByIdAsync(id);
            CreateUpdateProduct = mapper.Map<CreateUpdateProduct>(ProductDto);
            return Page();
        }

        public async Task<IActionResult> OnPostAsync(int id)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return Page();
                }

                Category = await categoryService.GetListAsync().ToListAsync();
                Manufacturer = await manufacturerService.GetListAsync().ToListAsync();
                Variations = await variationService.GetListAsync().ToListAsync();
                CreateUpdateProduct.Variations = VariationJson;
                ProductDto = await productService.UpdateProductAsync(id, CreateUpdateProduct);
                if (ProductDto != null)
                {
                    StatusMessage = $"Bạn vừa update: {ProductDto.Name}";
                    return RedirectToPage("./Index");
                }
                return Page();
            }
            catch (Exception ex)
            {
                ViewData["ErrorMessage"] = ex.Message;
            }
            return Page();  
        }
    }
}
