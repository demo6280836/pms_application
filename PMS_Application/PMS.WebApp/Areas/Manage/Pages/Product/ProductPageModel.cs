﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using PMS.Contract.Commons.UnitOfWorks;
using PMS.Contract.Contents.Categorys;
using PMS.Contract.Contents.Manufacturers;
using PMS.Contract.Contents.Variations;
using PMS.Contract.Systems.Products;

namespace PMS.WebApp.Areas.Manage.Pages.Product;

public class ProductPageModel : PageModel
{
    protected readonly IProductService productService;
    protected readonly ICategoryService categoryService;
    protected readonly IManufacturerService manufacturerService;
    protected readonly IVariationService variationService;
    protected readonly IUnitOfWork unitOfWork;
    [TempData]
    public string StatusMessage { get; set; }
    public ProductPageModel(
        IProductService productService, 
        ICategoryService categoryService,
        IManufacturerService manufacturerService,
        IUnitOfWork unitOfWork,
        IVariationService variationService)
    {
        this.productService = productService;
        this.categoryService = categoryService;
        this.manufacturerService = manufacturerService;
        this.unitOfWork = unitOfWork;
        this.variationService = variationService;
    }

    public ProductPageModel(IProductService productService, ICategoryService categoryService, IManufacturerService manufacturerService, IUnitOfWork unitOfWork)
    {
        this.productService = productService;
        this.categoryService = categoryService;
        this.manufacturerService = manufacturerService;
        this.unitOfWork = unitOfWork;
    }
}
