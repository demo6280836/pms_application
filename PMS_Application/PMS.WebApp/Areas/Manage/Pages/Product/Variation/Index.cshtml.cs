using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using PMS.Contract.Commons.UnitOfWorks;
using PMS.Contract.Contents.Variations;
using PMS.Domain.Contents;

namespace PMS.WebApp.Areas.Manage.Pages.Product.Variation
{
    public class IndexModel : PageModel
    {
        private readonly IVariationService variationService;
        private readonly IUnitOfWork unitOfWork;

        [TempData]
        public string StatusMessage { get; set; }

        public IndexModel(
            IVariationService variationService,
            IUnitOfWork unitOfWork)
        {
            this.variationService = variationService;
            this.unitOfWork = unitOfWork;
        }

        [BindProperty(SupportsGet = true)]
        public List<PMS.Domain.Contents.Variation> Variations { get; set; }
        [BindProperty]
        public PMS.Domain.Contents.Variation VariationRequest { get; set; }

        public async Task<IActionResult> OnGetAsync()
        {
            Variations = variationService.GetListAsync().ToList();
            return Page();
        }

        public async Task<IActionResult> OnPostAsync()
        {
            if(ModelState.IsValid)
            {
                var result = await variationService.InsertAsync(VariationRequest);
                if (result != null)
                {
                    StatusMessage = "Create successfully";
                    await unitOfWork.SaveChangesAsync();
                    return RedirectToPage("Index");
                }
            }
            return Page();
        }
    }
}
