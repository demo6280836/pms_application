﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using PMS.Application.Contents;
using PMS.Contract.Commons.UnitOfWorks;
using PMS.Contract.Contents.Categorys;
using PMS.Contract.Contents.Manufacturers;

namespace PMS.WebApp.Areas.Manage.Pages.Manufacturer
{
    public class DeleteModel : ManufacturerPageModel
    {
        public DeleteModel(IManufacturerService manufacturerService, IUnitOfWork unitOfWork) : base(manufacturerService, unitOfWork)
        {
        }

        public PMS.Domain.Contents.Manufacturer Manufacturer { get; set; }


        public async Task<IActionResult> OnGetAsync(int id)
        {
            Manufacturer = await manufacturerService.GetByIdAsync(id);
            if (Manufacturer == null)
            {
                return NotFound("Not Found Category");
            }

            return Page();
        }

        public async Task<IActionResult> OnPostAsync(int id)
        {
            Manufacturer = await manufacturerService.GetByIdAsync(id);
            if (Manufacturer == null)
            {
                return NotFound("Not Found Category");
            }

            var result = manufacturerService.DeleteAsync(Manufacturer);
            await unitOfWork.SaveChangesAsync();
            if (result != null)
            {
                StatusMessage = $"Bạn vừa xóa: {Manufacturer.Name}";
                return RedirectToPage("./Index");
            }
            return Page();
        }
    }
}
