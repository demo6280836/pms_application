﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using PMS.Contract.Commons.UnitOfWorks;
using PMS.Contract.Contents.Manufacturers;

namespace PMS.WebApp.Areas.Manage.Pages.Manufacturer
{
    public class ManufacturerPageModel : PageModel
    {
        protected readonly IManufacturerService manufacturerService;
        protected readonly IUnitOfWork unitOfWork;
        [TempData]
        public string StatusMessage { get; set; }
        public ManufacturerPageModel(IManufacturerService manufacturerService, IUnitOfWork unitOfWork)
        {
            this.manufacturerService = manufacturerService;
            this.unitOfWork = unitOfWork;
        }
    }
}
