using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using PMS.Application.Contents;
using PMS.Contract.Commons.UnitOfWorks;
using PMS.Contract.Contents.Manufacturers;

namespace PMS.WebApp.Areas.Manage.Pages.Manufacturer
{
    public class CreateModel : ManufacturerPageModel
    {
        public CreateModel(IManufacturerService manufacturerService, IUnitOfWork unitOfWork) : base(manufacturerService, unitOfWork)
        {
        }

        [BindProperty]
        public CreateUpdateManufacturer Request { get; set; }


        public async Task<IActionResult> OnPostAsync()
        {
            if (!ModelState.IsValid)
            {
                return Page();
            }


            var newManufacturer = new PMS.Domain.Contents.Manufacturer
            {
                Code = Request.Code,
                Name = Request.Name,
                Address= Request.Address,
                Email= Request.Email,
                Phone= Request.Phone,  
            };
            var result = await manufacturerService.InsertAsync(newManufacturer);
            await unitOfWork.SaveChangesAsync();

            if (result != null)
            {
                return RedirectToPage("./Index");
            }

            return Page();
        }
    }
}
