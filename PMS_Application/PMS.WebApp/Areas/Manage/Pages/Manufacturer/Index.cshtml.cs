using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using PMS.Contract.Commons.UnitOfWorks;
using PMS.Contract.Contents.Manufacturers;

namespace PMS.WebApp.Areas.Manage.Pages.Manufacturer
{
    public class IndexModel : ManufacturerPageModel
    {
        public IndexModel(
            IManufacturerService manufacturerService,
            IUnitOfWork unitOfWork) 
            : base(manufacturerService, unitOfWork)
        {
        }
        [BindProperty(SupportsGet = true)]
        public ManufacturerResponse Manufacturers { get; set; }
        [BindProperty(SupportsGet = true)]
        public ManufacturerRequest Request { get; set; }
        public void OnGet()
        {
            Manufacturers = manufacturerService.GetManufacturers(Request);
        }
    }
}
