﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using PMS.Application.Contents;
using PMS.Contract.Commons.UnitOfWorks;
using PMS.Contract.Contents.Manufacturers;

namespace PMS.WebApp.Areas.Manage.Pages.Manufacturer
{
    public class EditModel : ManufacturerPageModel
    {
        public EditModel(IManufacturerService manufacturerService, IUnitOfWork unitOfWork) : base(manufacturerService, unitOfWork)
        {
        }
        [BindProperty]
        public CreateUpdateManufacturer Request { get; set; }
        public PMS.Domain.Contents.Manufacturer Manufacturer { get; set; }

        public async Task<IActionResult> OnGetAsync(int id)
        {
            Manufacturer = await manufacturerService.GetByIdAsync(id);
            if (Manufacturer== null)
            {
                return NotFound("Not Found Category");
            }

            Request = new CreateUpdateManufacturer
            {
                Code = Manufacturer.Code,
                Address= Manufacturer.Address,
                Email= Manufacturer.Email,  
                Name= Manufacturer.Name,
                Phone = Manufacturer.Phone 
            };

            return Page();
        }

        public async Task<IActionResult> OnPostAsync(int id)
        {
            Manufacturer = await manufacturerService.GetByIdAsync(id);
            if (Manufacturer == null)
            {
                return NotFound("Not Found Category");
            }
            if (!ModelState.IsValid)
            {
                return Page();
            }

            Manufacturer.Name = Request.Name;
            Manufacturer.Phone = Request.Phone;
            Manufacturer.Address = Request.Address;
            Manufacturer.Email = Request.Email;

            var result = await manufacturerService.UpdateAsync(Manufacturer);
            await unitOfWork.SaveChangesAsync();

            if (result != null)
            {
                StatusMessage = $"Bạn vừa đổi: {Request.Name}";
                return RedirectToPage("./Index");
            }

            return Page();
        }
    }
}
