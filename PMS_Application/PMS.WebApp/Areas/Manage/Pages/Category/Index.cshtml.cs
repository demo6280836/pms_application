using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using PMS.Application.Contents;
using PMS.Contract.Commons.UnitOfWorks;
using PMS.Contract.Contents.Categorys;
using PMS.WebApp.Areas.Manage.Pages.Category;

namespace PMS.WebApp.Areas.Manage.Pages.Inventory.Category
{
    public class IndexModel : CategoryPageModel
    {

        public IndexModel(
            ICategoryService categoryService,
            IUnitOfWork unitOfWork) 
            : base(categoryService, unitOfWork)
        {
        }

        [BindProperty]
        public CategoryResponse Categories { get; set; }
        [BindProperty(SupportsGet = true)]
        public CategoryRequest Request { get; set; }
        public void OnGet()
        {
            Categories = categoryService.GetCategories(Request);
        }
    }
}
