﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using PMS.Contract.Commons.UnitOfWorks;
using PMS.Contract.Contents.Categorys;
using PMS.Domain.Contents;

namespace PMS.WebApp.Areas.Manage.Pages.Category
{
    public class EditModel : CategoryPageModel
    {
        public EditModel(
            ICategoryService categoryService, 
            IUnitOfWork unitOfWork) 
            : base(categoryService, unitOfWork)
        {
        }

        [BindProperty]
        public CreateUpdateCategory Request { get; set; }
        public PMS.Domain.Contents.Category Category { get; set; }

        public async Task<IActionResult> OnGetAsync(int id)
        {
            Category = await categoryService.GetByIdAsync(id);
            if (Category == null)
            {
                return NotFound("Not Found Category");
            }

            Request = new CreateUpdateCategory
            {
                Name = Category.Name,
                ParentId = Category.ParentId,
            };

            return Page();
        }

        public async Task<IActionResult> OnPostAsync(int id)
        {
            Category = await categoryService.GetByIdAsync(id);
            if (Category == null)
            {
                return NotFound("Not Found Category");
            }
            if (!ModelState.IsValid)
            {
                return Page();
            }

            Category.Name = Request.Name;
            Category.ParentId = Request.ParentId;
            var result = await categoryService.UpdateAsync(Category);
            await unitOfWork.SaveChangesAsync();

            if (result != null)
            {
                StatusMessage = $"Bạn vừa đổi tên: {Request.Name}";
                return RedirectToPage("./Index");
            }

            return Page();
        }


    }
}
