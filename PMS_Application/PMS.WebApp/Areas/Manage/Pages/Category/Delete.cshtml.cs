﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using PMS.Contract.Commons.UnitOfWorks;
using PMS.Contract.Contents.Categorys;

namespace PMS.WebApp.Areas.Manage.Pages.Category
{
    public class DeleteModel : CategoryPageModel
    {
        public DeleteModel(ICategoryService categoryService, IUnitOfWork unitOfWork) : base(categoryService, unitOfWork)
        {
        }

        public PMS.Domain.Contents.Category Category { get; set; }

        public async Task<IActionResult> OnGetAsync(int id)
        {
            Category = await categoryService.GetByIdAsync(id);
            if (Category == null)
            {
                return NotFound("Not Found Category");
            }

            return Page();
        }

        public async Task<IActionResult> OnPostAsync(int id)
        {
            Category = await categoryService.GetByIdAsync(id);
            if (Category == null)
            {
                return NotFound("Not Found Category");
            }

            var result = categoryService.DeleteAsync(Category);
            await unitOfWork.SaveChangesAsync();
            if (result != null)
            {
                StatusMessage = $"Bạn vừa xóa: {Category.Name}";
                return RedirectToPage("./Index");
            }
            return Page();
        }
    }
}
