using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using PMS.Contract.Commons.UnitOfWorks;
using PMS.Contract.Contents.Categorys;
using PMS.Domain.Contents;

namespace PMS.WebApp.Areas.Manage.Pages.Category
{
    public class CreateModel : CategoryPageModel
    {
        public CreateModel(
            ICategoryService categoryService,
            IUnitOfWork unitOfWork) 
            : base(categoryService, unitOfWork)
        {
        }

        [BindProperty]
        public CreateUpdateCategory Request { get; set; }

        public async Task<IActionResult> OnPostAsync()
        {
            if (!ModelState.IsValid)
            {
                return Page();
            }


            var newCategory = new PMS.Domain.Contents.Category
            {
                Name= Request.Name,
                ParentId= Request.ParentId,
            };
            var result = await categoryService.InsertAsync(newCategory);
            await unitOfWork.SaveChangesAsync();

            if (result != null)
            {
                return RedirectToPage("./Index");
            }

            return Page();
        }
    }
}
