﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using PMS.Contract.Commons.UnitOfWorks;
using PMS.Contract.Contents.Categorys;

namespace PMS.WebApp.Areas.Manage.Pages.Category;

public class CategoryPageModel : PageModel
{
	protected readonly ICategoryService categoryService;
	protected readonly IUnitOfWork unitOfWork;
    [TempData]
    public string StatusMessage { get; set; }
    public CategoryPageModel(ICategoryService categoryService, IUnitOfWork unitOfWork)
	{
		this.categoryService = categoryService;
		this.unitOfWork = unitOfWork;
	}
}
