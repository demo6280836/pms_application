using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Newtonsoft.Json;
using PMS.Contract.Contents.Order;
using PMS.Contract.Systems.Products;
using PMS.Domain.Contents;
using PMS.Infrastructure.EnityFrameworkCore;

namespace PMS.WebApp.Areas.Manage.Pages.PointOfSale
{
    public class IndexModel : PointOfSalePageModel
    {
        public List<ProductOrderRequest> SelectedProducts { get; set; } = new List<ProductOrderRequest>();
        

        public IndexModel(
            IProductService productService,
            IOrderService orderService,
            PMSDbContext context) 
            : base(productService, orderService,context)
        {
            ProductResponse = new ProductResponse();
        }

        [BindProperty(SupportsGet = true)]
        public ProductResponse ProductResponse { get; set; }
        [BindProperty(SupportsGet = true)]
        public ProductRequest ProductSearchRequest { get; set; }

        public async Task OnGetAsync()
        {
           ProductResponse = await productService.ProductsAsync(ProductSearchRequest);
        }

        
        [BindProperty]
        public string SelectedProductsString { get; set; }
        
        [BindProperty(SupportsGet = true)]
        public OrderRequest OrderRequest { get; set; }
        public async Task<IActionResult> OnPostAsync()
        {
            ProductResponse = await productService.ProductsAsync(ProductSearchRequest);
            if (SelectedProductsString == null)
            {
                throw new Exception("Khong co san pham");
            }
            SelectedProducts = JsonConvert.DeserializeObject<List<ProductOrderRequest>>(SelectedProductsString);

            if (SelectedProducts != null)
            {
                try
                {
                    await orderService.OrderProductAsync(SelectedProducts, OrderRequest);
                    StatusMessage = $"Order successfully";
                }
                catch (Exception ex)
                {
                    ViewData["ErrorMessage"] = ex.Message;
                }
            }
            return Page();
        }
    }
}
