﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using PMS.Contract.Contents.Order;
using PMS.Contract.Systems.Products;
using PMS.Infrastructure.EnityFrameworkCore;

namespace PMS.WebApp.Areas.Manage.Pages.PointOfSale;

public class PointOfSalePageModel : PageModel
{
    protected readonly IProductService productService;
	protected readonly IOrderService orderService;
	protected readonly PMSDbContext context;

    [TempData]
    public string StatusMessage { get; set; }
    public PointOfSalePageModel(
		IProductService productService,
		IOrderService orderService,
		PMSDbContext context)
	{
		this.productService= productService;
		this.context= context;
		this.orderService= orderService;
	}
}
