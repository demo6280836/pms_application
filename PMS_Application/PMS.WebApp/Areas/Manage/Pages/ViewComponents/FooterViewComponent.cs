﻿using Microsoft.AspNetCore.Mvc;

namespace PMS.WebApp.Areas.Manage.Pages.ViewComponents
{
    public class FooterViewComponent : ViewComponent
    {
        public async Task<IViewComponentResult> InvokeAsync()
        {
            return View();
        }
    }

}
