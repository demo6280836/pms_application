﻿using Microsoft.AspNetCore.Mvc;

namespace PMS.WebApp.Areas.Manage.Pages.ViewComponents
{
    public class HeaderViewComponent : ViewComponent
    {
        public async Task<IViewComponentResult> InvokeAsync()
        {
            return View();
        }

    }
}
