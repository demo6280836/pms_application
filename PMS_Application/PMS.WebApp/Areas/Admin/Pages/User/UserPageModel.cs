﻿using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc;
using PMS.Infrastructure.EnityFrameworkCore;
using PMS.Domain.Systems;
using PMS.Contract.Systems.Users;

namespace PMS.WebApp.Areas.Admin.Pages.User
{
    public class UserPageModel : PageModel
    {
        protected readonly UserManager<AppUser> _userManager;
        protected readonly PMSDbContext _context;
        protected readonly IUserService _userService;

        [TempData]
        public string StatusMessage { get; set; }
        public UserPageModel(UserManager<AppUser> userManager, PMSDbContext context,IUserService userService)
        {
            _userManager = userManager;
            _context = context;
            _userService = userService;
        }
    }
}
