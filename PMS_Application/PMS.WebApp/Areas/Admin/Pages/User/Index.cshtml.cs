using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using PMS.Contract.Systems.Users;
using PMS.Domain.Systems;
using PMS.Infrastructure.EnityFrameworkCore;


namespace PMS.WebApp.Areas.Admin.Pages.User
{
    public class IndexModel : UserPageModel
    {
        public IndexModel(UserManager<AppUser> userManager, PMSDbContext context, IUserService userService) : base(userManager, context, userService)
        {
        }
        public UserResponse UserResponse { get; set; }
        [BindProperty(SupportsGet = true)]
        public UserRequest UserRequest { get; set; }

        public async Task OnGet()
        {
            UserResponse = await _userService.GetListAllAsync(UserRequest);
        }
        public void OnPost() => RedirectToPage();
    }
}
