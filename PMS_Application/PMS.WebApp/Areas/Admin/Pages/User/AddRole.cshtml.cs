using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using PMS.Contract.Systems.Users;
using PMS.Domain.Systems;
using PMS.Infrastructure.EnityFrameworkCore;
using System.ComponentModel;

namespace PMS.WebApp.Areas.Admin.Pages.User
{
    public class AddRoleModel : UserPageModel
    {

        private readonly RoleManager<IdentityRole> _roleManager;
        public AddRoleModel(UserManager<AppUser> userManager, 
            PMSDbContext context,
            IUserService userService,
            RoleManager<IdentityRole> roleManager) : base(userManager, context, userService)
        {
            _roleManager = roleManager;
        }

        public UserDto user { get; set; }

        [BindProperty]
        [DisplayName("C�c role g�n cho user")]
        public IList<string> Roles { get; set; }

        public SelectList AllRoles { get; set; }

        public async Task<IActionResult> OnGetAsync(string id)
        {
            if (string.IsNullOrEmpty(id))
            {
                return NotFound($"Not found user");
            }

            user = await _userService.GetUserByIdAsync(id);

            if (user == null)
            {
                return NotFound($"Not found user");
            }

            Roles = user.Roles;

            List<string?> roleNames = await _roleManager.Roles.Select(r => r.Name).ToListAsync();
            AllRoles = new SelectList(roleNames);

            return Page();
        }

        public async Task<IActionResult> OnPostAsync(string id)
        {
            if (string.IsNullOrEmpty(id))
            {
                return NotFound($"Not found user");
            }

            user = await _userService.GetUserByIdAsync(id);

            if (user == null)
            {
                return NotFound($"Not found user");
            }

            await _userService.AssignRolesAsync(id, Roles.ToArray());

            List<string?> roleNames = await _roleManager.Roles.Select(r => r.Name).ToListAsync();
            AllRoles = new SelectList(roleNames);



            StatusMessage = $"Update role for {user.UserName} successfully";

            return RedirectToPage("./Index");
        }
    }
}

