using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using PMS.Application.Constants;
using PMS.Contract.Systems.Roles;
using PMS.Infrastructure.EnityFrameworkCore;

namespace PMS.WebApp.Areas.Admin.Pages.Role
{
    [Authorize(Permissions.Roles.View)]
    public class IndexModel : RolePageModel
    {
        public IndexModel(RoleManager<IdentityRole> roleManager, PMSDbContext context) : base(roleManager, context)
        {
        }

        public List<IdentityRole> Roles { get; set; }

        public async Task OnGet()
        {
            Roles = await _roleManager.Roles.ToListAsync();
        }
    }
}
