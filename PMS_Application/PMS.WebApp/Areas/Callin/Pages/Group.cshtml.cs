﻿using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using PMS.Contract.Commons.UnitOfWorks;
using PMS.Contract.Signalr.Groups;
using PMS.Contract.Signalr.Messages;
using PMS.Contract.Systems.Users;
using PMS.Domain.Signalr;
using PMS.Domain.Systems;
using PMS.Infrastructure.EnityFrameworkCore;

namespace PMS.WebApp.Areas.Callin.Pages
{
    public class GroupModel : PageModel
    {
        private readonly PMSDbContext _context;
        private readonly IMapper _mapper;
        private readonly IUnitOfWork _unitOfWork;
        private readonly IUserService _userService;
        private readonly IGroupService _groupService;
		private readonly IHttpContextAccessor _httpContextAccessor;

		public GroupModel(PMSDbContext context,
           IMapper mapper,
           IUnitOfWork unitOfWork,
           IUserService userService,
           IGroupService groupService,
           IMessageService messageService
           )
        {
            _context = context;
            _mapper = mapper;
            _unitOfWork = unitOfWork;
            _userService = userService;
            _groupService = groupService;
        }
        [BindProperty(SupportsGet = true)]
        public List<GroupDto> GroupDtos { get; set; }
        [BindProperty(SupportsGet = true)]
        public UserDto CurrentUser { get; set; }  //User hiện tại
		[BindProperty(SupportsGet = true)]
		public CreateUpdateGroupDto CreateUpdateGroup { get; set; }

        [BindProperty]
        public int GroupId { get; set; }

        public async Task<IActionResult> OnGet()
        {

            var user = _context.Users.FirstOrDefault(u => u.UserName == User.Identity.Name);
            CurrentUser = _mapper.Map<AppUser, UserDto>(user);

			//var groups = _context.Groups.Include(x => x.Messages).Include(x => x.GroupUsers).ToList();

			var groups = _context.Groups.Where(g => g.GroupUsers.Any(gu => gu.UserId == user.Id)).Include(x => x.Messages).Include(x => x.GroupUsers).ToList();

			GroupDtos = _mapper.Map<List<GroupDto>>(groups);
			return Page();
        }

        public async Task<IActionResult> OnPostCreateGroup()
        {
            var createGroup = _mapper.Map<Group>(CreateUpdateGroup);
            await _groupService.InsertAsync(createGroup);
            await _unitOfWork.SaveChangesAsync();

            var userCreated = _context.Users.Where(x => x.UserName == createGroup.CreatedBy).FirstOrDefault();
            _context.GroupUsers.Add(new GroupUser
            {   
                UserId = userCreated.Id,
                GroupId = createGroup.Id,
                IsKey = true,
            });
			await _unitOfWork.SaveChangesAsync();

			return RedirectToPage();
        }

		public async Task<IActionResult> OnPostDeleteGroup()
		{
            var group = await _groupService.GetByIdAsync(GroupId);
			await _groupService.DeleteAsync(group);
			await _unitOfWork.SaveChangesAsync();


			return RedirectToPage();
		}

       
	}
}
