﻿using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using PMS.Contract.Commons.UnitOfWorks;
using PMS.Contract.Signalr.Groups;
using PMS.Contract.Signalr.Messages;
using PMS.Contract.Systems.Users;
using PMS.Domain.Signalr;
using PMS.Domain.Systems;
using PMS.Infrastructure.EnityFrameworkCore;
using System.Text.RegularExpressions;


namespace PMS.WebApp.Areas.Callin.Pages
{
    public class IndexModel : PageModel
    {
        private readonly PMSDbContext _context;
        private readonly IMapper _mapper;
        private readonly IUnitOfWork _unitOfWork;
        private readonly IUserService _userService;
        private readonly IGroupService _groupService;
        private readonly IMessageService _messageService;

        public IndexModel(PMSDbContext context,
            IMapper mapper,
            IUnitOfWork unitOfWork,
            IUserService userService,
            IGroupService groupService,
            IMessageService messageService)
        {
            _context = context;
            _mapper = mapper;
            _unitOfWork = unitOfWork;
            _userService = userService;
            _groupService = groupService;
            _messageService = messageService;
        }

		[BindProperty]
		public List<UserDto> Others { get; set; }

        [BindProperty]
        public UserDto CurrentUser { get; set; }  //User hiện tại

        [BindProperty(SupportsGet = true)]
        public List<UserDto> UserDtos { get; set; }  //List user cuar group hiện tại


        [BindProperty(SupportsGet = true)]
        public List<MessageDto> MessageDtos { get; set; }  // List message of Current group

		[BindProperty(SupportsGet = true)]
		public int CurrentGroupId { get; set; }  //Current groupId

        [BindProperty(SupportsGet = true)]
        public GroupDto CurrentGroup { get; set; }
        [BindProperty]
        public string AddOtherId { get; set; }

        public async Task<IActionResult> OnGet(int groupId)
        {
            CurrentGroupId = groupId;
            var group = _context.Groups.Where(x => x.Id == groupId).Include(x => x.Messages).Include(x => x.GroupUsers).FirstOrDefault();
            CurrentGroup = _mapper.Map<GroupDto>(group);


			var messages = _context.Messages.Include(m => m.Sender).Where(m => m.GroupId == CurrentGroupId).OrderBy(m => m.CreationTime).ToList();
			MessageDtos = _mapper.Map<List<Message>, List<MessageDto>>(messages);

			var users = _context.Users.Where(u => u.GroupUsers.Any(gu => gu.GroupId == groupId))
                                .Include(x => x.GroupUsers)
                                .ThenInclude(x => x.Group)
                                .ToList();
            UserDtos = _mapper.Map<List<UserDto>>(users);

            var user = _context.Users.FirstOrDefault(u => u.UserName == User.Identity.Name);
            CurrentUser = _mapper.Map<AppUser, UserDto>(user);

			// Lấy danh sách tất cả người dùng
			var allUsers = _context.Users.ToList();

			var usersInGroup = users.Select(m => m.Id).ToList();

			// Lấy danh sách các người dùng không thuộc vào group hiện tại
			Others = _mapper.Map<List<UserDto>>(allUsers.Where(u => !usersInGroup.Contains(u.Id)).ToList());


			return Page();
           
        }

        public async  Task<IActionResult> OnPost()
        {
			var messages = _context.Messages.Include(m => m.Sender).Where(m => m.GroupId == CurrentGroupId).OrderBy(m => m.CreationTime).ToList();
			MessageDtos = _mapper.Map<List<Message>, List<MessageDto>>(messages);
			return RedirectToPage();
		}

		public async Task<IActionResult> OnPostAddOther()
		{
            _context.GroupUsers.Add(new GroupUser
            {
                GroupId = CurrentGroupId,
                UserId = AddOtherId,
                IsKey = false,
            });
            await _unitOfWork.SaveChangesAsync();

			return RedirectToPage(new { groupId = CurrentGroupId });
		}
	}
}
