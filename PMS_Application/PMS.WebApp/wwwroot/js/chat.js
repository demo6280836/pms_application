﻿//Connect
var connection = new signalR.HubConnectionBuilder().withUrl("/chatHub").build();


//Start
connection.start().then(function () {
    console.log('SignalR Started...')
}).catch(function (err) {
    return console.error(err.toString());
});

connection.on("getProfileInfo", function (userDto) {
    console.log("Received user info:", userDto);

});

connection.on("ReceiveMessage", function (groupId, message) {
    var grId = document.getElementById("groupId").value;
    if (grId == groupId) {
        document.getElementById("messageBox").innerHTML += message;
    }
});



document.getElementById("sendButton").addEventListener("click", function (event) {
    var groupId = document.getElementById("groupId").value;
    var username = document.getElementById("currentUser").value;
    var message = document.getElementById("messageInput").value;
    connection.invoke("SendMessage", groupId, message, username ).catch(function (err) {
        return console.error(err.toString());
    });
    document.getElementById("messageInput").value = "";
    event.preventDefault();
});



$(document).ready(function () {
    $("#members-button").click(function () {
        // Ẩn danh sách Others
        $(".others-list").hide();
        // Hiển thị danh sách Members
        $(".members-list").show();
    });

    $("#others-button").click(function () {
        // Ẩn danh sách Members
        $(".members-list").hide();
        // Hiển thị danh sách Others
        $(".others-list").show();
    });
});

$(document).ready(function () {
    $("#members-button").click(function () {

        $(".members-icon").addClass("archived");
        $(".others-icon").removeClass("archived");
    });

    $("#others-button").click(function () {

        $(".others-icon").addClass("archived");
        $(".members-icon").removeClass("archived");
    });
});