﻿using AutoMapper;
using ClosedXML.Excel;
using DocumentFormat.OpenXml.InkML;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using PMS.Contract.Systems.Products;
using PMS.Domain.Contents;
using PMS.Infrastructure.EnityFrameworkCore;
using System.Data;

namespace PMS.WebApp.Controllers
{
	[Route("api/[controller]")]
	[ApiController]
	public class ExtensionController : Controller
	{
		private readonly PMSDbContext _context;
		private readonly IMapper _mapper;
        public ExtensionController(PMSDbContext context,IMapper mapper)
        {
            _context = context;
			_mapper = mapper;
        }

		[HttpGet("category")]
		public async Task<FileResult> ExportCategoryInExcel()
		{
			var categories = await _context.Categories.ToListAsync();
			var fileName = "category.xlsx";
			return GenerateCategoryExcel(fileName, categories);
		}
		private FileResult GenerateCategoryExcel(string fileName, IEnumerable<Category> categories)
		{
			DataTable dataTable = new DataTable("Category");
			dataTable.Columns.AddRange(new DataColumn[]
			{
				new DataColumn("Id"),
				new DataColumn("Name")
			});

			foreach (var category in categories)
			{
				dataTable.Rows.Add(category.Id, category.Name);
			}

			using (XLWorkbook wb = new XLWorkbook())
			{
				wb.Worksheets.Add(dataTable);
				using (MemoryStream stream = new MemoryStream())
				{
					wb.SaveAs(stream);

					return File(stream.ToArray(),
						"application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
						fileName);
				}
			}

		}

		[HttpGet("product")]
		public async Task<FileResult> ExportProductInExcel()
		{
			var products = await _context.Products.Include(x => x.Category).Include(x => x.Manufacturer).ToListAsync();
			var productDtos = _mapper.Map<List<ProductDto>>(products);
			var fileName = "product.xlsx";
			return GenerateProductExcel(fileName, productDtos);
		}

		private FileResult GenerateProductExcel(string fileName, IEnumerable<ProductDto> productDtos)
		{
			DataTable dataTable = new DataTable("Product");
			dataTable.Columns.AddRange(new DataColumn[]
			{
				new DataColumn("Code"),
				new DataColumn("Name"),
				new DataColumn("Category"),
				new DataColumn("Manufacturer"),
				new DataColumn("Quantity"),
				new DataColumn("Price"),
				new DataColumn("Production Date"),
				new DataColumn("Expire date"),

			});

			foreach (var productDto in productDtos)
			{
				dataTable.Rows.Add(productDto.Code, productDto.Name,productDto.CategoryName,productDto.ManufacturerName,productDto.Quantity,productDto.Price,productDto.DateOfManufacture,productDto.Expiry);
			}

			using (XLWorkbook wb = new XLWorkbook())
			{
				wb.Worksheets.Add(dataTable);
				using (MemoryStream stream = new MemoryStream())
				{
					wb.SaveAs(stream);

					return File(stream.ToArray(),
						"application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
						fileName);
				}
			}

		}
	}
}
