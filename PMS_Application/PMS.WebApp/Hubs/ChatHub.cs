﻿using AutoMapper;
using Humanizer;
using Microsoft.AspNetCore.SignalR;
using Microsoft.EntityFrameworkCore;
using PMS.Contract.Commons.UnitOfWorks;
using PMS.Contract.Signalr.Messages;
using PMS.Contract.Systems.Users;
using PMS.Domain.Signalr;
using PMS.Domain.Systems;
using PMS.Infrastructure.EnityFrameworkCore;
using System;

namespace PMS.WebApp.Hubs
{
	public class ChatHub : Hub
	{
		public readonly static List<UserDto> _connections = new List<UserDto>();
		private readonly static Dictionary<string, string> _connectionMaps = new Dictionary<string, string>();

		private string IdentityName
		{
			get { return Context.User.Identity.Name; }
		}

		private readonly PMSDbContext _context;
		private readonly IMapper _mapper;
		private readonly IUnitOfWork _unitOfWork;
		public ChatHub(PMSDbContext context, IMapper mapper, IUnitOfWork unitOfWork)
		{
			_context = context;
			_mapper = mapper;
			_unitOfWork = unitOfWork;
		}

		public async Task SendMessage(string groupId, string content, string username)
		{
			AppUser user = _context.Users.FirstOrDefault(a => a.UserName == username);

			if (user != null)
			{
				var createMessageDto = new CreateMessageDto
				{
					GroupId = int.Parse(groupId),
					Content = content,
					SenderId = user.Id,
				};
				var message = _mapper.Map<Message>(createMessageDto);
				_context.Messages.Add(message);
				await _unitOfWork.SaveChangesAsync();

				List<GroupUser> groupUsers = _context.GroupUsers.Include(x => x.User).Where(g => g.GroupId == int.Parse(groupId)).ToList();
				List<string> group = new List<string>();
				// Duyệt qua danh sách thành viên và lọc ra các thành viên mà bạn muốn gửi thông điệp đến
				foreach (GroupUser groupUser in groupUsers)
				{
					if (groupUser.User != null && groupUser.User.UserName != username)
					{
						group.Add(groupUser.User.UserName);
					}
				}

				var contentHtml = $"<div class=\"d-flex flex-row-reverse mb-2\">\r\n" +
					"<div class=\"right-chat-message fs-13 mb-2\">\r\n" +
					"<div class=\"mb-0 mr-3 pr-4\">\r\n" +
					"<div class=\"d-flex flex-row\">\r\n" +
					"<div class=\"mb-3 mr-5 pr-5\">"+content+"</div>\r\n" +
					"<div class=\"pr-4\"></div>\r\n" +
					"</div>\r\n</div>\r\n<div class=\"message-options dark\">\r\n" +
					"<div class=\"message-time\">\r\n" +
					"<div class=\"d-flex flex-row\">\r\n" +
					"<div class=\"mr-2\">"+message.CreationTime.Humanize(utcDate: false) + "</div>\r\n<div class=\"svg15 double-check\"></div>\r\n\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"message-arrow\"><i class=\"text-muted la la-angle-down fs-17\"></i></div>\r\n\t\t\t\t\t\t\t\t\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t\t\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t\t\t\t\t\t\t</div>";


				// Gửi thông điệp đến người gửi
				await Clients.Caller.SendAsync("ReceiveMessage", groupId, contentHtml);

				contentHtml = $"\t<div class=\"left-chat-message fs-13 mb-2\">\r\n\t\t\t\t\t\t\t\t\t\t\t\t\t<p class=\"mb-3 mr-5 pr-5\">"+content+ "</p>\r\n\t\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"message-options\">\r\n\t\t\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"message-time\">From "+ user.FullName+":" +message.CreationTime.Humanize(utcDate: false) +"</div>\r\n\t\t\t\t\t\t\t\t\t\t\t\t\t</div>\r\n\r\n\t\t\t\t\t\t\t\t\t\t\t\t</div>";

				// Gửi thông điệp đến các thành viên khác trong nhóm chat
				//await Clients.Users(group).SendAsync("ReceiveMessage", groupId, contentHtml);
				await Clients.Others.SendAsync("ReceiveMessage", groupId, contentHtml);
			}
		}

		//public async Task Join(string groupName)
		//{
		//	try
		//	{
		//		var userDto = _connections.Where(u => u.UserName == IdentityName).FirstOrDefault();
		//		if (userDto != null && userDto.CurrentGroup != groupName)
		//		{
		//			// Remove user from others list
		//			if (!string.IsNullOrEmpty(userDto.CurrentGroup))
		//				await Clients.OthersInGroup(userDto.CurrentGroup).SendAsync("removeUser", userDto);

		//			// Join to new chat room
		//			await Leave(userDto.CurrentGroup);
		//			await Groups.AddToGroupAsync(Context.ConnectionId, groupName);
		//			userDto.CurrentGroup = groupName;

		//			// Tell others to update their list of users
		//			await Clients.OthersInGroup(groupName).SendAsync("addUser", userDto);
		//		}
		//	}
		//	catch (Exception ex)
		//	{
		//		await Clients.Caller.SendAsync("onError", "You failed to join the chat room!" + ex.Message);
		//	}
		//}

		public async Task Leave(string groupName)
		{
			await Groups.RemoveFromGroupAsync(Context.ConnectionId, groupName);
		}


		public override Task OnConnectedAsync()
		{
			try
			{
				var user = _context.Users.Where(u => u.UserName == IdentityName).FirstOrDefault();
				var userDto = _mapper.Map<AppUser, UserDto>(user);
				

				if (!_connections.Any(u => u.UserName == IdentityName))
				{
					_connections.Add(userDto);
					_connectionMaps.Add(IdentityName, Context.ConnectionId);
				}

				Clients.Caller.SendAsync("getProfileInfo", userDto);
			}
			catch (Exception ex)
			{
				Clients.Caller.SendAsync("onError", "OnConnected:" + ex.Message);
			}
			return base.OnConnectedAsync();
		}

		public override Task OnDisconnectedAsync(Exception exception)
		{
			try
			{
				var user = _connections.Where(u => u.UserName == IdentityName).First();
				_connections.Remove(user);

				// Tell other users to remove you from their list
				//Clients.OthersInGroup(user.CurrentGroup).SendAsync("removeUser", user);

				// Remove mapping
				_connectionMaps.Remove(user.UserName);
			}
			catch (Exception ex)
			{
				Clients.Caller.SendAsync("onError", "OnDisconnected: " + ex.Message);
			}

			return base.OnDisconnectedAsync(exception);
		}

	}
}
