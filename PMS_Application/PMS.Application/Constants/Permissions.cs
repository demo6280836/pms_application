﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PMS.Application.Constants
{
	public static class Permissions
	{
		public static class Dashboard
		{
			[Description("View dashboard")]
			public const string View = "Permissions.Dashboard.View";
		}
		public static class Roles
		{
			[Description("View role")]
			public const string View = "Permissions.Roles.View";
			[Description("Add new role")]
			public const string Create = "Permissions.Roles.Create";
			[Description("Edit role")]
			public const string Edit = "Permissions.Roles.Edit";
			[Description("Delete role")]
			public const string Delete = "Permissions.Roles.Delete";
		}
		public static class Users
		{
			[Description("View user")]
			public const string View = "Permissions.Users.View";
			[Description("Add new user")]
			public const string Create = "Permissions.Users.Create";
			[Description("Edit user")]
			public const string Edit = "Permissions.Users.Edit";
			[Description("Delete user")]
			public const string Delete = "Permissions.Users.Delete";
		}

	}
}
