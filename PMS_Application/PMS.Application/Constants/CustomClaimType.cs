﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PMS.Application.Constants
{
	public static class CustomClaimType
	{
		public const string Uid = "uid";
		public const string Roles = "roles";
		public const string Permissions = "permissions";
		public const string FullName = "fullName";
		public const string UserName = "userName";

	}
}
