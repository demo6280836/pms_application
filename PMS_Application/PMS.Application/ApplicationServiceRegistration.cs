﻿using PMS.BusinessService.Common.UnitOfWorks;
using Microsoft.Extensions.DependencyInjection;
using System.Reflection;
using PMS.Contract.Commons.UnitOfWorks;
using PMS.BusinessService.Systems;
using PMS.Contract.Systems.Authentications;
using PMS.Contract.Systems.Users;
using PMS.Contract.Systems.Roles;
using IMS.BusinessService.Systems;
using PMS.Contract.Commons.Files;
using PMS.Application.Commons.Files;
using PMS.Contract.Systems.Products;
using PMS.Application.Contents;
using PMS.Contract.Contents.Categorys;
using PMS.Contract.Contents.Manufacturers;
using PMS.Contract.Signalr.Groups;
using PMS.Application.Signalr;
using PMS.Contract.Signalr.Messages;
using PMS.Contract.Contents.Variations;
using PMS.Contract.Signalr.Groups;
using PMS.Application.Signalr;
using PMS.Contract.Signalr.Messages;
using PMS.Contract.Contents.Order;

namespace PMS.Application;

public static class ApplicationServiceRegistration
{
    public static IServiceCollection ConfigureApplicationServices(this IServiceCollection services)
    {
        services.AddAutoMapper(Assembly.GetExecutingAssembly());
		services.AddScoped<IFileService, FileService>();
		//AuthService
		services.AddScoped<IAuthService, AuthService>();
		services.AddScoped<IUserService, UserService>();
		services.AddScoped<IRoleService, RoleService>();

		//Generic Repo
		services.AddScoped(typeof(IGenericRepository<>), typeof(GenericRepository<>));
		services.AddScoped<IUnitOfWork, UnitOfWork>();

		services.AddScoped<IProductService, ProductService>();
		services.AddScoped<ICategoryService, CategoryService>();
		services.AddScoped<IManufacturerService, ManufactuerService>();

		//Signalr
		services.AddScoped<IGroupService, GroupService>();
		services.AddScoped<IMessageService, MessageService>();

		services.AddScoped<IVariationService, VariationService>();

		//Signalr
		services.AddScoped<IGroupService, GroupService>();
		services.AddScoped<IMessageService, MessageService>();

		services.AddScoped<IOrderService, OrderService>();

		return services;
    }
}
