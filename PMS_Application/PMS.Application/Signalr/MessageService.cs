﻿using AutoMapper;
using PMS.Application.Commons;
using PMS.Contract.Signalr.Groups;
using PMS.Contract.Signalr.Messages;
using PMS.Domain.Signalr;
using PMS.Infrastructure.EnityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PMS.Application.Signalr
{
	public class MessageService : ServiceBase<Message>, IMessageService
	{
		public MessageService(PMSDbContext context, IMapper mapper) : base(context, mapper)
		{
		}
	}
}
