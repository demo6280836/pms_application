﻿using AutoMapper;
using PMS.Application.Commons;
using PMS.Contract.Signalr.Groups;
using PMS.Domain.Signalr;
using PMS.Infrastructure.EnityFrameworkCore;


namespace PMS.Application.Signalr
{
	public class GroupService : ServiceBase<Group> , IGroupService
	{
		public GroupService(PMSDbContext context, IMapper mapper) : base(context, mapper)
		{
		}
	}
}
