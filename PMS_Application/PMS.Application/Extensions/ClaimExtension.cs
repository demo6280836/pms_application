﻿
using Microsoft.AspNetCore.Identity;
using PMS.Contract.Systems.Roles;
using PMS.Domain.Systems;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Reflection;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace IMS.BusinessService.Extension
{
    public static class ClaimExtensions
    {
        // Phương thức mở rộng (extension method) để lấy danh sách quyền từ một lớp chính sách cụ thể và thêm chúng vào danh sách allPermissions.
        public static void GetPermissions(this List<RoleClaimDto> allPermissions, Type policy)
        {
            // Sử dụng reflection để lấy danh sách các trường tĩnh và công khai từ lớp chính sách (policy).
            FieldInfo[] fields = policy.GetFields(BindingFlags.Static | BindingFlags.Public);

            // Duyệt qua từng trường trong danh sách trường lấy được.
            foreach (FieldInfo fi in fields)
            {
                // Lấy tất cả các thuộc tính có kiểu DescriptionAttribute của trường (nếu có).
                var attribute = fi.GetCustomAttributes(typeof(DescriptionAttribute), true);

                // Lấy tên hiển thị ban đầu từ giá trị của trường.
                string displayName = fi.GetValue(null).ToString();

                // Nếu có thuộc tính DescriptionAttribute, thay đổi tên hiển thị thành giá trị của DescriptionAttribute.
                var attributes = fi.GetCustomAttributes(typeof(DescriptionAttribute), true);
                if (attributes.Length > 0)
                {
                    var description = (DescriptionAttribute)attribute[0];
                    displayName = description.Description;
                }

                // Thêm quyền vào danh sách allPermissions dưới dạng một đối tượng RoleClaimDto.
                allPermissions.Add(new RoleClaimDto { Value = fi.GetValue(null).ToString(), Type = "Permission", DisplayName = displayName });
            }
        }

        // Phương thức mở rộng (extension method) để thêm một quyền làm Claim cho một vai trò trong IdentityRole.
        public static async Task AddPermissionClaim(this RoleManager<IdentityRole> roleManager, IdentityRole role, string permission)
        {
            // Lấy tất cả các Claim của vai trò.
            var allClaims = await roleManager.GetClaimsAsync(role);

            // Kiểm tra xem quyền đã tồn tại cho vai trò hay chưa. Nếu chưa, thêm quyền làm Claim.
            if (!allClaims.Any(a => a.Type == "Permission" && a.Value == permission))
            {
                await roleManager.AddClaimAsync(role, new Claim("Permission", permission));
            }
        }
    }

}
