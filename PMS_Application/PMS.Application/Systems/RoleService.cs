﻿using AutoMapper;
using IMS.BusinessService.Extension;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using PMS.Application.Commons;
using PMS.Application.Constants;
using PMS.Contract.Common.Sorting;
using PMS.Contract.Systems.Roles;
using PMS.Domain.Systems;
using PMS.Infrastructure.EnityFrameworkCore;
using System.Linq.Dynamic.Core;
using System.Reflection;


namespace IMS.BusinessService.Systems
{
	public class RoleService : ServiceBase<IdentityRole>, IRoleService
	{
		private readonly RoleManager<IdentityRole> _roleManager;
		private readonly UserManager<AppUser> _userManager;

		public RoleService(UserManager<AppUser> userManager,
			RoleManager<IdentityRole> roleManager,
			IMapper mapper,
			PMSDbContext context)
			: base(context, mapper)
		{
			_userManager = userManager;
			_roleManager = roleManager;
		}

		public async Task AddRole(CreateUpdateRoleDto input)
		{
			if (await _roleManager.RoleExistsAsync(input.Name))
			{
				throw new Exception("Role name is existed");
			}
			await _roleManager.CreateAsync(new IdentityRole(input.Name.Trim()));
		}

		public async Task DeleteManyRole(string[] ids)
		{
			foreach (var id in ids)
			{
				var role = await _roleManager.FindByIdAsync(id.ToString());
				if (role == null) if (role == null) throw new Exception();
				await _roleManager.DeleteAsync(role);
			}
		}

		

		public async Task<RoleResponse> GetListAllPaging(RoleRequest request)
		{
			var roles = await _roleManager.Roles
				.Where(x => string.IsNullOrWhiteSpace(request.KeyWords)
					|| x.Name.Contains(request.KeyWords))
				.ToListAsync();

			var rolePaging = roles.Paginate(request);
			var roleDtos = mapper.Map<List<RoleDto>>(rolePaging);

			var response = new RoleResponse
			{
				Roles = roleDtos,
				Page = GetPagingResponse(request, roles.Count()),
			};
			return response;
		}

		public async Task<RoleDto> GetRoleById(string roleId)
		{
			var role = await _roleManager.FindByIdAsync(roleId.ToString());
			if (role == null) throw new Exception("Not found");
			return mapper.Map<IdentityRole, RoleDto>(role);
		}

	
		public async Task UpdateRole(string id, CreateUpdateRoleDto input)
		{
			var role = await _roleManager.FindByIdAsync(id.ToString());
			if (role == null) throw new Exception("Not found");

			role.Name = input.Name;

			await _roleManager.UpdateAsync(role);
		}

        public async Task<PermissionDto> GetAllRolePermission(string roleId)
        {
            // Tạo một đối tượng PermissionDto để lưu trữ thông tin về quyền.
            var model = new PermissionDto();

            // Tạo một danh sách allPermissions để lưu trữ tất cả các quyền.
            var allPermissions = new List<RoleClaimDto>();

            // Lấy tất cả các lớp con (nested types) được định nghĩa trong lớp Permissions.
            var types = typeof(Permissions).GetTypeInfo().DeclaredNestedTypes;
            foreach (var type in types)
            {
                // Gọi phương thức GetPermissions để lấy tất cả quyền từ lớp con hiện tại và thêm vào danh sách allPermissions.
                allPermissions.GetPermissions(type);
            }

            // Tìm và kiểm tra vai trò theo roleId.
            var role = await _roleManager.FindByIdAsync(roleId);
            if (role == null)
            {
                throw new Exception("Not found");
            }

            // Lưu trữ roleId vào đối tượng model.
            model.RoleId = roleId;

            // Lấy tất cả các quyền (claims) của vai trò.
            var claims = await _roleManager.GetClaimsAsync(role);

            // Tạo danh sách allClaimValues chứa giá trị của tất cả quyền.
            var allClaimValues = allPermissions.Select(a => a.Value).ToList();

            // Tạo danh sách roleClaimValues chứa giá trị của quyền của vai trò.
            var roleClaimValues = claims.Select(a => a.Value).ToList();

            // Tìm các quyền đã được phân quyền bằng cách tìm sự giao nhau giữa danh sách allClaimValues và roleClaimValues.
            var authorizedClaims = allClaimValues.Intersect(roleClaimValues).ToList();

            // Đánh dấu quyền đã được phân quyền trong danh sách allPermissions.
            foreach (var permission in allPermissions)
            {
                if (authorizedClaims.Any(a => a == permission.Value))
                {
                    permission.Selected = true;
                }
            }

            // Lưu danh sách quyền vào đối tượng model và trả về model.
            model.RoleClaims = allPermissions;
            return model;
        }


        public async Task SavePermission(PermissionDto input)
		{
			var role = await _roleManager.FindByIdAsync(input.RoleId);
			if (role == null) throw new Exception("Not found");

			var claims = await _roleManager.GetClaimsAsync(role);
			foreach (var claim in claims)
			{
				await _roleManager.RemoveClaimAsync(role, claim);
			}
			var selectedClaims = input.RoleClaims.Where(a => a.Selected).ToList();
			foreach (var claim in selectedClaims)
			{
				await _roleManager.AddPermissionClaim(role, claim.Value);
			}
		}

        public async Task<List<RoleDto>> GetListAllAsync()
        {
            var roles = await _roleManager.Roles.ToListAsync();
            var roleDtos = mapper.Map<List<RoleDto>>(roles);
            return roleDtos;


        }
    }
}
