﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using PMS.Application.Commons;
using PMS.Contract.Contents.Categorys;
using PMS.Domain.Contents;
using PMS.Infrastructure.EnityFrameworkCore;

namespace PMS.Application.Contents;

public class CategoryService : ServiceBase<Category>, ICategoryService
{
    public CategoryService(PMSDbContext context, IMapper mapper) : base(context, mapper)
    {
    }

    public CategoryResponse GetCategories(CategoryRequest request)
    {
        var categories = context.Categories
            .Include(x => x.CategoryParent)
            .Where(x => string.IsNullOrEmpty(request.KeyWords)
                    || x.Name.Contains(request.KeyWords))
            .ToList();
        var categoriesMap = mapper.Map<List<CategoryDto>>(categories);

        return new CategoryResponse
        {
            Categories = categoriesMap,
            Page = GetPagingResponse(request, categoriesMap.Count)
        };
    }
}
