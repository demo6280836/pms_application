﻿using AutoMapper;
using PMS.Application.Commons;
using PMS.Contract.Contents.Variations;
using PMS.Domain.Contents;
using PMS.Infrastructure.EnityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PMS.Application.Contents;

public class VariationService : ServiceBase<Variation>, IVariationService
{
    public VariationService(PMSDbContext context, IMapper mapper) : base(context, mapper)
    {
    }

}
