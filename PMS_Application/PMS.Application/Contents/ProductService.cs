﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using Microsoft.IdentityModel.Tokens;
using PMS.Application.Commons;
using PMS.Contract.Commons.UnitOfWorks;
using PMS.Contract.Contents.Variations;
using PMS.Contract.Systems.Products;
using PMS.Domain.Contents;
using PMS.Infrastructure.EnityFrameworkCore;
using System.Text.Json;
using static Microsoft.EntityFrameworkCore.DbLoggerCategory.Model;

namespace PMS.Application.Contents;

public class ProductService : ServiceBase<Product>, IProductService
{
    private readonly IUnitOfWork unitOfWork;
    public ProductService(
        PMSDbContext context,
        IMapper mapper,
        IUnitOfWork unitOfWork)
        : base(context, mapper)
    {
        this.unitOfWork = unitOfWork;
    }

    public async Task<ProductDto> CreateProductAsync(CreateUpdateProduct request)
    {
        if (request == null)
        {
            throw new ArgumentNullException(nameof(request), "The request object is null.");
        }

        // check validation product
        ValidateProduct(request);

        var entity = mapper.Map<Product>(request);
        await context.Products.AddAsync(entity);
        try
        {
            var result = await unitOfWork.SaveChangesAsync();
            if (result == 1)
            {
                if (!string.IsNullOrEmpty(request.Variations))
                {
                    var productVariations = DeserializeProductVariations(request.Variations, entity.Id);
                    await context.ProductVariations.AddRangeAsync(productVariations);
                    await unitOfWork.SaveChangesAsync();
                }
                return mapper.Map<ProductDto>(entity);
            }
        }
        catch (DbUpdateException ex)
        {
            throw new Exception("An error occurred while saving the product.", ex);
        }

        return null;
    }

    public async Task<ProductDto> GetProductByIdAsync(int id)
    {
        var entity = await context.Products
            .Include(x => x.ProductVariations)
            .ThenInclude(x => x.Variation)
            .FirstOrDefaultAsync(x => x.Id == id);

        if (entity == null)
        {
            throw new Exception("Khong tim thay san pham");
        }

        var productDto = mapper.Map<ProductDto>(entity);
        productDto.ProductVariations = new List<ProductVariationResponse>();
        foreach (var item in entity.ProductVariations)
        {
            productDto.ProductVariations.Add(new ProductVariationResponse
            {
                VariationId = item.VariationId,
                Name = item.Variation.Name,
                Value = item.Value,
            });
        }

        return productDto;
    }

    public async Task<ProductResponse> ProductsAsync(ProductRequest request)
    {
        var products = await context.Products
            .Include(x => x.Category)
            .Include(x => x.Manufacturer)
            .Include(x => x.ProductVariations)
                .ThenInclude(x => x.Variation)
            .Where(x =>
            string.IsNullOrWhiteSpace(request.KeyWords)
            || x.Name.Contains(request.KeyWords)
            || x.Code.Contains(request.KeyWords)
            || x.Category.Name.Contains(request.KeyWords)
            || x.Manufacturer.Name.Contains(request.KeyWords)
            && (!request.CategoryId.HasValue
                || request.CategoryId == 0
                || x.CategoryId == request.CategoryId)
            && (!request.ManufacturerId.HasValue
                || request.ManufacturerId == 0
                || x.ManufacturerId == request.ManufacturerId))
            .OrderByDescending(x => x.CreationTime)
            .ThenByDescending(x => x.Code)
            .Select(p => new ProductDto
            {
                Id = p.Id,
                Code = p.Code,
                Name = p.Name,
                CategoryId = p.CategoryId,
                ManufacturerId = p.ManufacturerId,
                Quantity = p.Quantity,
                DateOfManufacture = p.ProductionDate.Value,
                Expiry = p.ExpireDate,
                Price = p.Price,
                Image = p.Image,
                Discount = p.Discount,
                CategoryName = p.Category.Name,
                ManufacturerName = p.Manufacturer.Name,
                ProductVariations = p.ProductVariations.Select(pv => new ProductVariationResponse
                {
                    Name = pv.Variation.Name,
                    Value = pv.Value
                }).ToList()
            })
            .ToListAsync();
        return new ProductResponse
        {
            Products = products,
            Page = GetPagingResponse(request, products.Count)
        };
    }

    public async Task<ProductDto> UpdateProductAsync(int id, CreateUpdateProduct request)
    {
        var productEntity = await context.Products
            .Include(x => x.ProductVariations)
            .FirstOrDefaultAsync(x => x.Id == id);
        if (productEntity is null)
        {
            throw new NullReferenceException("Khong tim thay product");
        }
        // check validation product
        ValidateProduct(request);

        UpdateProductVariations(productEntity, request.Variations);

        mapper.Map(request, productEntity);
        context.Products.Update(productEntity);
        await unitOfWork.SaveChangesAsync();
        return mapper.Map<ProductDto>(productEntity);

    }

    private List<ProductVariation> DeserializeProductVariations(string variationJson, int productId)
    {
        var variations = JsonSerializer.Deserialize<List<VariationResponse>>(variationJson)
                        ?? new List<VariationResponse>();
        return variations.Select(x => new ProductVariation
        {
            ProductId = productId,
            VariationId = int.Parse(x.id),
            Value = x.value,
        }).ToList();
    }


    // update product variation
    private void UpdateProductVariations(Product product, string variationsJson)
    {
        if (string.IsNullOrEmpty(variationsJson))
        {
            return;
        }

        var productVariations = DeserializeProductVariations(variationsJson, product.Id);
        RemoveObsoleteVariations(product, productVariations);
        UpdateOrAddNewVariations(product, productVariations);
    }

    // check validation create product
    private void ValidateProduct(CreateUpdateProduct request)
    {
        if (request.ExpireDate <= request.ProductionDate)
        {
            throw new Exception("Expire date must be greater than the production date.");
        }
        if (request.Price <= 0 || request.Quantity <= 0)
        {
            throw new Exception("Price and quantity must be greater than 0.");
        }
    }

    private void RemoveObsoleteVariations(Product product, List<ProductVariation> newVariations)
    {
        var newVariationIds = new HashSet<int>(newVariations.Select(v => v.VariationId));
        var variationsToDelete = product.ProductVariations
            .Where(v => !newVariationIds.Contains(v.VariationId))
            .ToList();

        foreach (var variation in variationsToDelete)
        {
            context.ProductVariations.Remove(variation);
        }
    }

    private void UpdateOrAddNewVariations(Product product, List<ProductVariation> newVariations)
    {
        foreach (var variation in newVariations)
        {
            var existingVariation = product.ProductVariations
                .FirstOrDefault(v => v.VariationId == variation.VariationId);

            if (existingVariation != null)
            {
                existingVariation.Value = variation.Value;
            }
            else
            {
                context.ProductVariations.Add(variation);
            }
        }
    }
}
