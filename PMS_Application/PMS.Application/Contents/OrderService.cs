﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using Microsoft.IdentityModel.Tokens;
using PMS.Application.Commons;
using PMS.Contract.Commons.UnitOfWorks;
using PMS.Contract.Contents.Order;
using PMS.Contract.Systems.Products;
using PMS.Domain.Contents;
using PMS.Infrastructure.EnityFrameworkCore;

namespace PMS.Application.Contents;

public class OrderService : ServiceBase<Order>, IOrderService
{
    private readonly IUnitOfWork unitOfWork;
    private readonly IProductService productService;
    public OrderService(
        IUnitOfWork unitOfWork,
        PMSDbContext context, 
        IProductService productService,
        IMapper mapper) 
        : base(context, mapper)
    {
        this.unitOfWork = unitOfWork;
        this.productService = productService;
    }

    public async Task<OrderDto> OrderProductAsync(
        IList<ProductOrderRequest> productOrdersRequest, 
        OrderRequest orderDtoRequest)
    {
        if (productOrdersRequest == null
            || orderDtoRequest.TotalPrice < 0
            || orderDtoRequest.PaymentCustomer < 0
            || orderDtoRequest.TotalPrice > orderDtoRequest.PaymentCustomer)
            throw new Exception("Not enough money");

        // tao ra order
        var orderEntity = mapper.Map<Order>(orderDtoRequest);
        if (orderEntity.Discount != 0)
        {
            orderEntity.Total = orderEntity.Total*(1 - orderEntity.Discount);
        }
        await context.Orders.AddAsync(orderEntity);

        var saveOrderResult = await unitOfWork.SaveChangesAsync();
        if (saveOrderResult <= 0)
            throw new Exception("Luu khong thanh cong");
        // ket thuc tao order

        foreach (var item in productOrdersRequest)
        {
            var validationProducts 
                = GetProductsByCodeAndCheckQuantity(item.Code, item.Quantity);
            var orderItemsResponse 
                = await UpdateNumberProductAsync(validationProducts, item.Quantity);
            
            var orderItemEntities = mapper.Map<List<OrderItem>>(orderItemsResponse);
            orderItemEntities.ForEach(item =>
            {
                item.OrderId = orderEntity.Id;
            });
            await context.OrderItems.AddRangeAsync(orderItemEntities);
            var result = await unitOfWork.SaveChangesAsync();

            if (result <= 0) throw new Exception("Luu khong thanh cong");

        }
        var orderDto = mapper.Map<OrderDto>(orderEntity);

        return orderDto;
    }

    private async Task<List<ProductOrderResponse>> UpdateNumberProductAsync(List<ProductDto> validationProductOrders, int quantityOrder)
    {
        var productOrder = new List<ProductOrderResponse>();  // luu nhung sp duoc lay
        if (validationProductOrders == null)
            throw new Exception("Khong co san pham");
        foreach(var item in validationProductOrders)
        {
            int quantityToReduce = Math.Min(item.Quantity, quantityOrder);
            item.Quantity -= quantityToReduce;
            quantityOrder -= quantityToReduce;
            var productEntity = await productService.GetByIdAsync(item.Id);
            productEntity.Quantity = item.Quantity;
            await productService.UpdateAsync(productEntity);

            productOrder.Add(new ProductOrderResponse
            {
                ProductId = item.Id,
                ProductCode= item.Code,
                Price= item.Price,
                Quantity= quantityToReduce
            });
            if (quantityOrder == 0)
                break;
        }

        return productOrder;  
    }

    public List<ProductDto> GetProductsByCodeAndCheckQuantity(string code, int quantityOrder)
    {
        var products = context.Products
            .Where(x => x.Code.Equals(code)
            && x.IsActive != false)
            .ToList();

        if (products.Any(x => DBNull.Value.Equals(x.ExpireDate)))
        {
            products = products.Where(x => x.ExpireDate > DateTime.Now)
                .OrderBy(x => x.ExpireDate)
                .ToList();   
        }

        int totalQuantity = 0;
        products.ForEach(x =>
        {
            totalQuantity += x.Quantity;
        });
        if (totalQuantity < quantityOrder)
            throw new Exception("Khong du san pham");

        var productDtos = mapper.Map<List<ProductDto>>(products);
        return productDtos;
    }
}
