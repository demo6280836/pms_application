﻿using AutoMapper;
using PMS.Application.Commons;
using PMS.Contract.Contents.Manufacturers;
using PMS.Domain.Contents;
using PMS.Infrastructure.EnityFrameworkCore;

namespace PMS.Application.Contents;

public class ManufactuerService : ServiceBase<Manufacturer>, IManufacturerService
{
    public ManufactuerService(
        PMSDbContext context, 
        IMapper mapper)
        : base(context, mapper)
    {
    }

    public ManufacturerResponse GetManufacturers(ManufacturerRequest request)
    {
        var manufacturers = context.Manufacturers.ToList();

        var manufacturersMap = mapper.Map<List<ManufacturerDto>>(manufacturers);

        return new ManufacturerResponse
        {
            Manufacturers= manufacturersMap,
            Page = GetPagingResponse(request, manufacturersMap.Count)
        };
    }
}
