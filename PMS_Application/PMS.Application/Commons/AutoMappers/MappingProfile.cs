﻿using AutoMapper;
using Microsoft.AspNetCore.Identity;
using PMS.Application.Commons.AutoMappers.Resolvers;
using PMS.Contract.Contents.Categorys;
using PMS.Contract.Contents.Manufacturers;
using PMS.Contract.Contents.Order;
using PMS.Contract.Signalr.Groups;
using PMS.Contract.Signalr.Messages;
using PMS.Contract.Systems.Products;
using PMS.Contract.Systems.Roles;
using PMS.Contract.Systems.Users;
using PMS.Domain.Contents;
using PMS.Domain.Signalr;
using PMS.Domain.Systems;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PMS.Application.Commons.AutoMappers
{
	public class MappingProfile : Profile
	{
        public MappingProfile()
        {
			//User
			CreateMap<IdentityRole, RoleDto>().ReverseMap();
			CreateMap<AppUser, UserDto>().ReverseMap();
				//.ForMember(dest => dest.CurrentGroup, opt => opt.MapFrom<CurrentGroupResolver>());


			//User
			CreateMap<CreateUserDto, AppUser>().ReverseMap();
			CreateMap<UpdateUserDto, AppUser>().ReverseMap()
				.ForMember(dest => dest.PhoneNumber, opt => opt.MapFrom(src => src.PhoneNumber))
				.ForMember(dest => dest.Avatar, opt => opt.MapFrom(src => src.Avatar));

			// Product
			CreateMap<Product, ProductDto>()
				.ForMember(dest => dest.CategoryName, opt => opt.MapFrom(src => src.Category.Name))
				.ForMember(dest => dest.ManufacturerName, opt => opt.MapFrom(src => src.Manufacturer.Name))
				.ForMember(dest => dest.DateOfManufacture, opt => opt.MapFrom(src => src.ProductionDate))
				.ForMember(dest => dest.Expiry, opt => opt.MapFrom(src => src.ExpireDate))
				.ForMember(dest => dest.ProductVariations, opt => opt.Ignore())
                .ReverseMap();
			CreateMap<Product, CreateUpdateProduct>().ReverseMap();
			CreateMap<ProductDto, CreateUpdateProduct>()
				.ForMember(dest => dest.ProductionDate, opt => opt.MapFrom(src => src.DateOfManufacture))
                .ForMember(dest => dest.ExpireDate, opt => opt.MapFrom(src => src.Expiry))
                .ReverseMap();

			CreateMap<Order, OrderDto>().ReverseMap();
			CreateMap<OrderItem, OrderItemDto>().ReverseMap();
			CreateMap<OrderRequest, Order>()
				.ForMember(dest => dest.Total, opt => opt.MapFrom(src => src.TotalPrice))
				.ReverseMap();
			CreateMap<ProductOrderResponse, OrderItem>().ReverseMap();
			

            // Category
            CreateMap<Category, CategoryDto>().ReverseMap();
			CreateMap<Category, CreateUpdateCategory>().ReverseMap();

			CreateMap<Manufacturer, ManufacturerDto>().ReverseMap();

			//Message
			CreateMap<CreateMessageDto, Message>().ReverseMap();

			CreateMap<MessageDto, Message>().ReverseMap()
				.ForMember(dest => dest.Username, opt => opt.MapFrom(src => src.Sender.UserName));

			//Group
			CreateMap<CreateUpdateGroupDto, Group>().ReverseMap();
			CreateMap<GroupDto, Group>().ReverseMap()
				.ForMember(dest => dest.LastMassage, opt => opt.MapFrom<LastMessageResolver>());



		}
	}
}
