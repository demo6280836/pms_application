﻿using AutoMapper;
using PMS.Contract.Signalr.Groups;
using PMS.Domain.Signalr;

namespace PMS.Application.Commons.AutoMappers.Resolvers
{
	public class LastMessageResolver : IValueResolver<Group, GroupDto, string>
	{
		public string Resolve(Group source, GroupDto destination, string destMember, ResolutionContext context)
		{
			var messages = source.Messages.OrderByDescending(x => x.CreationTime).ToList();
			if (messages.Count > 0)
			{
				return messages[0].Content;

			}
			else
			{
				return "";
			}
		}
	}
}
