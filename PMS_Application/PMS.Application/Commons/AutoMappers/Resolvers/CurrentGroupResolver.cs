﻿using AutoMapper;
using PMS.Contract.Systems.Users;
using PMS.Domain.Signalr;
using PMS.Domain.Systems;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static PMS.Application.Constants.Permissions;

namespace PMS.Application.Commons.AutoMappers.Resolvers
{
	public class CurrentGroupResolver : IValueResolver<AppUser, UserDto, string>
	{
		public string Resolve(AppUser source, UserDto destination, string destMember, ResolutionContext context)
		{
			// Lấy danh sách các nhóm (groups) mà người dùng tham gia
			List<Group> userGroups = source.GroupUsers.Select(ug => ug.Group).ToList();

			if (userGroups.Count > 0)
			{
				Group firstGroup = userGroups[0];
				return firstGroup.Name;
			}
			else
			{
				return "";
			}
		}
	}
}
