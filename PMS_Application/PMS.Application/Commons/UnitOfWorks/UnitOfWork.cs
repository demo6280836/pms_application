﻿
using PMS.Contract.Commons.UnitOfWorks;
using PMS.Infrastructure.EnityFrameworkCore;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PMS.Application.Constants;

namespace PMS.BusinessService.Common.UnitOfWorks
{
	public class UnitOfWork : IUnitOfWork
	{
		private readonly PMSDbContext _context;


		private readonly IHttpContextAccessor _httpContextAccessor;

		//private IProductRepository _productRepository;


		public UnitOfWork(PMSDbContext context, IHttpContextAccessor httpContextAccessor)
		{
			_context = context;
			this._httpContextAccessor = httpContextAccessor;
		}

		//public IProductRepository ProductRepository => _productRepository ?? new ProductRepository();



		public void Dispose()
		{
			_context.Dispose();
			GC.SuppressFinalize(this);
		}

		public async Task<int> SaveChangesAsync()
		{
			var username = _httpContextAccessor.HttpContext.User.Identity.Name;

			var result = await _context.SaveChangesAsync(username);
			return result;
		}
	}
}
