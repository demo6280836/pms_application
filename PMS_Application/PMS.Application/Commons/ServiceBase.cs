﻿using AutoMapper;
using PMS.BusinessService.Common.UnitOfWorks;
using PMS.Contract.Commons.Paging;
using PMS.Infrastructure.EnityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PMS.Application.Commons
{
	public abstract class ServiceBase<T> : GenericRepository<T> where T : class
	{
		protected readonly IMapper mapper;
		public ServiceBase(PMSDbContext context, IMapper mapper) : base(context)
		{
			this.mapper = mapper;
		}


		public static PagingResponseInfo GetPagingResponse(PagingRequestBase request, int totalRecord)
		{
			return new PagingResponseInfo
			{
				CurrentPage = request.Page,
				ItemsPerPage = request.ItemsPerPage,
				ToTalPage = (totalRecord / request.ItemsPerPage) + ((totalRecord % request.ItemsPerPage) == 0 ? 0 : 1),
				ToTalRecord = totalRecord
			};
		}
	}

}
