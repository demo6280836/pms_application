﻿using PMS.Domain.Systems;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection.Emit;
using System.Text;
using System.Threading.Tasks;

namespace PMS.Infrastructure.Seedings
{
	public static class SeedingDataIdentity
	{
		public static void SeedData(this ModelBuilder builder)
		{
			var roleUserId = "cac43a6e-f7bb-4448-baaf-1add431ccbbf";
			var roleAdminId = "cbc43a8e-f7bb-4445-baaf-1add431ffbbf";
			builder.Entity<IdentityRole>().HasData(
				new IdentityRole
				{
					Id = roleUserId,
					Name = "User",
					NormalizedName = "USER",
					ConcurrencyStamp = Guid.NewGuid().ToString()
				},
				new IdentityRole
				{
					Id = roleAdminId.ToString(),
					Name = "Admin",
					NormalizedName = "ADMIN",
					ConcurrencyStamp = Guid.NewGuid().ToString(),
				
				}
			);

			var hasher = new PasswordHasher<AppUser>();
			var adminId =  "8e445865-a24d-4543-a6c6-9443d048cdb9";
			var userId =  "9e224968-33e4-4652-b7b7-8574d048cdb9";
			builder.Entity<AppUser>().HasData(
				 new AppUser
				 {
					 Id = adminId,
					 Email = "admin@gmail.com",
					 NormalizedEmail = "ADMIN@GMAIL.COM",
					 FullName = "System",
					 UserName = "admin@gmail.com",
					 NormalizedUserName = "ADMIN@GMAIL.COM",
					 PasswordHash = hasher.HashPassword(null, "Abcd@1234"),
					 EmailConfirmed = true,
					 CreationTime = DateTime.UtcNow,
					 SecurityStamp = Guid.NewGuid().ToString(),
				 },
				 new AppUser
				 {
					 Id = userId,
					 Email = "user@gmail.com",
					 NormalizedEmail = "USER@GMAIL.COM",
					 FullName = "User",
					 UserName = "user@gmail.com",
					 NormalizedUserName = "USER@GMAIL.COM",
					 PasswordHash = hasher.HashPassword(null, "Abcd@1234"),
					 EmailConfirmed = true,
					 CreationTime = DateTime.UtcNow,
					 SecurityStamp = Guid.NewGuid().ToString(),
				 }
			);

			builder.Entity<IdentityUserRole<string>>().HasData(
				new IdentityUserRole<string>
				{
					RoleId = roleAdminId,
					UserId = adminId
				},
				new IdentityUserRole<string>
				{
					RoleId = roleUserId,
					UserId = userId
				}
				);
		}
	}
}
