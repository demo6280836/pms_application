﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using Microsoft.Extensions.Configuration;
using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PMS.Infrastructure.EnityFrameworkCore
{
	public class PMSDbContextFactory : IDesignTimeDbContextFactory<PMSDbContext>
	{
		public PMSDbContext CreateDbContext(string[] args)
		{

			var configuration = BuildConfiguration();

			var builder = new DbContextOptionsBuilder<PMSDbContext>()
				.UseSqlServer(configuration.GetConnectionString("Default"));

			return new PMSDbContext(builder.Options);
		}

		private static IConfigurationRoot BuildConfiguration()
		{
			var builder = new ConfigurationBuilder()
				.SetBasePath(Path.Combine(Directory.GetCurrentDirectory()))
				.AddJsonFile("appsettings.json", optional: false);


			return builder.Build();

		}
	}
}
