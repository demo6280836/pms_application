﻿using PMS.Domain.Abstracts;
using PMS.Domain.Systems;
using PMS.Infrastructure.Seedings;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.ChangeTracking;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection.Emit;
using System.Security.AccessControl;
using System.Text;
using System.Threading.Tasks;
using PMS.Domain.Contents;
using static System.Net.Mime.MediaTypeNames;
using PMS.Domain.Signalr;

namespace PMS.Infrastructure.EnityFrameworkCore
{
	public class PMSDbContext : IdentityDbContext<AppUser>
	{
       
        public DbSet<Inventory> Inventories { get; set; }
        public DbSet<Product> Products { get; set; }
        public DbSet<ProductVariation> ProductVariations { get; set; }
        public DbSet<Variation> Variations { get; set; }
        public DbSet<Manufacturer> Manufacturers { get; set; }
        public DbSet<Category> Categories { get; set; }
		public DbSet<Order> Orders { get; set; }
		public DbSet<OrderItem> OrderItems { get; set; }

		//Signalr
		public DbSet<GroupUser> GroupUsers { get; set; }
        public DbSet<Group> Groups { get; set; }
        public DbSet<Message> Messages { get; set; }
        public PMSDbContext(DbContextOptions options) : base(options)
		{
		}
		protected override void OnConfiguring(DbContextOptionsBuilder builder)
		{
			base.OnConfiguring(builder);
		}

		protected override void OnModelCreating(ModelBuilder builder)
		{

			base.OnModelCreating(builder);
			builder.SeedData();

			foreach (var entityType in builder.Model.GetEntityTypes())
			{
				var tableName = entityType.GetTableName();
				if (tableName.StartsWith("AspNet"))
				{
					entityType.SetTableName(tableName.Substring(6));
				}
			}
		}

		public virtual async Task<int> SaveChangesAsync(string username = "SYSTEM")
		{
			foreach (var entry in base.ChangeTracker.Entries<Auditable>()
				.Where(q => q.State == EntityState.Added || q.State == EntityState.Modified))
			{
				entry.Entity.LastModificationTime = DateTime.Now;
				entry.Entity.LastModifiedBy = username;

				if (entry.State == EntityState.Added)
				{
					entry.Entity.CreationTime = DateTime.Now;
					entry.Entity.CreatedBy = username;
				}

			}

			var entries = ChangeTracker.Entries().Where(e => e.State == EntityState.Added);
			foreach (var entityEntry in entries)
			{
				var dateCreatedProp = entityEntry.Entity.GetType().GetProperty("CreationTime");
				if (entityEntry.State == EntityState.Added
					&& dateCreatedProp != null)
				{
					dateCreatedProp.SetValue(entityEntry.Entity, DateTime.Now);
				}
			}

			var result = await base.SaveChangesAsync();

			return result;
		}

	}
}
