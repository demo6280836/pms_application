﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace PMS.Infrastructure.Migrations
{
    /// <inheritdoc />
    public partial class Update_Order : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<double>(
                name: "PaymentCustomer",
                table: "Orders",
                type: "float",
                nullable: false,
                defaultValue: 0.0);

            migrationBuilder.UpdateData(
                table: "Roles",
                keyColumn: "Id",
                keyValue: "cac43a6e-f7bb-4448-baaf-1add431ccbbf",
                column: "ConcurrencyStamp",
                value: "3ab89240-67cc-4934-a6d7-0fbfdce28975");

            migrationBuilder.UpdateData(
                table: "Roles",
                keyColumn: "Id",
                keyValue: "cbc43a8e-f7bb-4445-baaf-1add431ffbbf",
                column: "ConcurrencyStamp",
                value: "a2ad5c87-c202-42a2-aa89-d923fb749f0a");

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: "8e445865-a24d-4543-a6c6-9443d048cdb9",
                columns: new[] { "ConcurrencyStamp", "CreationTime", "PasswordHash", "SecurityStamp" },
                values: new object[] { "ac5a625f-4dc9-47e7-9ab6-e109ca58b72f", new DateTime(2023, 10, 25, 4, 4, 44, 59, DateTimeKind.Utc).AddTicks(4225), "AQAAAAIAAYagAAAAEGRH3c4BDXQmTuX0Zqru17jOHATdXyB4+WiD8FM0pABg4EtF6nAXuH1AI/XV0/fJmQ==", "de9b3cc1-ed45-465c-9ed1-c463d0990b35" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: "9e224968-33e4-4652-b7b7-8574d048cdb9",
                columns: new[] { "ConcurrencyStamp", "CreationTime", "PasswordHash", "SecurityStamp" },
                values: new object[] { "de50b3b2-ce30-408c-a6fe-2e6104ea9f20", new DateTime(2023, 10, 25, 4, 4, 44, 122, DateTimeKind.Utc).AddTicks(6450), "AQAAAAIAAYagAAAAEPxWyLJ6XnXyNGHspnqOXXiYFMUDe+yvSTJFUhnPCt9B/BeZNBY0/Lzcg1qRcb9iXA==", "5f1f62f3-928d-4dd4-8f0a-a6c379a2f853" });
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "PaymentCustomer",
                table: "Orders");

            migrationBuilder.UpdateData(
                table: "Roles",
                keyColumn: "Id",
                keyValue: "cac43a6e-f7bb-4448-baaf-1add431ccbbf",
                column: "ConcurrencyStamp",
                value: "e52177ce-58ec-48dc-9e57-5fd9bd0e9442");

            migrationBuilder.UpdateData(
                table: "Roles",
                keyColumn: "Id",
                keyValue: "cbc43a8e-f7bb-4445-baaf-1add431ffbbf",
                column: "ConcurrencyStamp",
                value: "a136350d-85cb-4491-85c9-254749fc5877");

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: "8e445865-a24d-4543-a6c6-9443d048cdb9",
                columns: new[] { "ConcurrencyStamp", "CreationTime", "PasswordHash", "SecurityStamp" },
                values: new object[] { "27ef77fe-b46f-45c2-b281-6585e45e5141", new DateTime(2023, 10, 22, 7, 43, 12, 349, DateTimeKind.Utc).AddTicks(9077), "AQAAAAIAAYagAAAAENfmz00RfiUhr6hjC58Eg137IJuXr7MFykWM6NvxeX4Bcg+Ej7l7i2hjyztNlZ7CSQ==", "5af16376-6cc6-42f0-b797-89b207c98ef7" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: "9e224968-33e4-4652-b7b7-8574d048cdb9",
                columns: new[] { "ConcurrencyStamp", "CreationTime", "PasswordHash", "SecurityStamp" },
                values: new object[] { "7bedfa16-8823-4e3f-ae88-c341412d155f", new DateTime(2023, 10, 22, 7, 43, 12, 415, DateTimeKind.Utc).AddTicks(1508), "AQAAAAIAAYagAAAAEKuUxN1jvKFFdh9mzRL/W424DmY8diIOWQ5GXs/Wp90a7Es98BvzIdlKl3ghYrYFNg==", "58259156-fc8e-44df-97cf-7d5db8eae416" });
        }
    }
}
