﻿using PMS.Domain.Abstracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PMS.Domain.Signalr
{
	public class Group : Auditable
	{
		public string Name { get; set; }
		public virtual ICollection<GroupUser> GroupUsers { get; set; }
		public virtual ICollection<Message> Messages { get; set; }
	}
}
