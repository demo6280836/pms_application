﻿using PMS.Domain.Abstracts;
using PMS.Domain.Contents;
using PMS.Domain.Systems;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PMS.Domain.Signalr
{
	public class GroupUser : Auditable
	{
		public string UserId { get; set; }
		public int GroupId { get; set; }
        public bool IsKey { get; set; }
		[ForeignKey(nameof(GroupId))]
		public virtual Group Group { get; set; }
		[ForeignKey(nameof(UserId))]
		public virtual AppUser User { get; set; }
	}
}
