﻿using PMS.Domain.Abstracts;
using PMS.Domain.Contents;
using PMS.Domain.Systems;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PMS.Domain.Signalr
{
	public class Message : Auditable
	{
        public string Content { get; set; }
        public string SenderId { get; set; }
        public int GroupId { get; set; }
		[ForeignKey(nameof(GroupId))]
		public virtual Group Group { get; set; }
		[ForeignKey(nameof(SenderId))]
		public virtual AppUser Sender { get; set; }
    }
}
