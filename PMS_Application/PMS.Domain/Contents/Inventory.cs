﻿using PMS.Domain.Abstracts;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PMS.Domain.Contents;

public class Inventory : Auditable
{
    public int ProductId { get; set; }
    public int StockQuantity { get; set; }
    [ForeignKey(nameof(ProductId))]
    public virtual Product Product { get; set; }
}
