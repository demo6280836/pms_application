﻿using PMS.Domain.Abstracts;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PMS.Domain.Contents;

public class ProductVariation : Auditable
{
    public int ProductId { get; set; }
    public int VariationId { get; set; }
    public string Value { get; set; }
    [ForeignKey(nameof(ProductId))]
    public virtual Product Product { get; set; }
    [ForeignKey(nameof(VariationId))]
    public virtual Variation Variation { get; set; }
}
