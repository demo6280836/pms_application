﻿using PMS.Domain.Abstracts;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PMS.Domain.Contents;

public class Product : Auditable
{
    public string Name { get; set; }
    public int? CategoryId { get; set; }
    public int? ManufacturerId { get; set; }
    public int Quantity { get; set; }
    public string Code { get; set; }
    public DateTime? ProductionDate { get; set; }
    public DateTime? ExpireDate { get; set; }
    public double Price { get; set; }
    public string? Image { get; set; }
    public double? Discount { get; set; }

    [ForeignKey(nameof(CategoryId))]
    public virtual Category? Category { get; set; }
    [ForeignKey(nameof(ManufacturerId))]
    public virtual Manufacturer? Manufacturer { get; set; }
    public virtual ICollection<ProductVariation> ProductVariations { get; set; }
    public virtual ICollection<OrderItem>? OrderItems { get; set; }
}
