﻿using PMS.Domain.Abstracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PMS.Domain.Contents;

public class Variation : Auditable
{
    public string Name { get; set; }
}
