﻿using PMS.Domain.Abstracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PMS.Domain.Contents
{
    public class Order : Auditable
    {
        public double Total { get; set; }
        public double Discount { get; set; }
        public double PaymentCustomer { get; set; }
        public string? CustomerName { get; set; }
        public string? CustomerPhoneNumber { get; set; }
        public string? CustomerAddress { get; set; }
        public ICollection<OrderItem>? OrderItems { get; set; }
    }
}
