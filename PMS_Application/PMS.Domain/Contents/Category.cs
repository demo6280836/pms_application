﻿using PMS.Domain.Abstracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PMS.Domain.Contents;

public class Category : Auditable
{
    public string Name { get; set; }
    public int? ParentId { get; set; }
    public virtual Category? CategoryParent { get; set; }
    public virtual ICollection<Product>? Products { get; set; }
}
