﻿using Microsoft.AspNetCore.Identity;
using PMS.Domain.Signalr;
using System.ComponentModel.DataAnnotations.Schema;


namespace PMS.Domain.Systems
{
	public class AppUser : IdentityUser
	{
		public string? FullName { set; get; }
		public string? Address { set; get; }
		public string? Avatar { get; set; }
		public DateTime? BirthDay { set; get; }
		public DateTime? CreationTime { get; set; }
		public virtual ICollection<GroupUser> GroupUsers { get; set; }
		public virtual ICollection<Message> Messages { get; set; }

	}
}
